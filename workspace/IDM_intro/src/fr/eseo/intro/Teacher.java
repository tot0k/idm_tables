package fr.eseo.intro;

public class Teacher extends User {
	
	public Teacher(String name) {
		super(name);
	}

	@Override
	public void addCourse(Course course) {
		super.addCourse(course);
		if (course.getTeacher() == null) {
			course.addTeacher(this);
		}
	}
}