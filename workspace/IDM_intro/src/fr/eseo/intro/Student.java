package fr.eseo.intro;

public class Student extends User {

	public Student(String name) {
		super(name);
	}

	@Override
	public void addCourse(Course course) {
		super.addCourse(course);
		if (!course.getStudents().contains(this)) {
			course.addStudent(this);
		}
	}
	
	@Override
	public void removeCourse(Course course) {
		super.addCourse(course);
		if (course.getStudents().contains(this)) {
			course.removeStudent(this);
		}
	}
	
}
