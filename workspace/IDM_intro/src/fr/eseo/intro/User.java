package fr.eseo.intro;

import java.util.ArrayList;
import java.util.List;

public abstract class User {
	public List<Course> courses;
	public String name;
	
	
	public User(String name) {
		this.courses = new ArrayList<Course>();
		this.name = name;
	}
	
	public void addCourse(Course course) {
		this.courses.add(course);
	}
	
	public void removeCourse(Course course) {
		this.courses.remove(course);
	}
	
	@Override
	public String toString() {
		return name;
	}
	
	public String getFormattedCourses() {
		String str = "";
		for (Course c : courses) {
			str += c.getName() + " - ";
		}
		return str;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public List<Course> getCourses() {
		return courses;
	}
}
