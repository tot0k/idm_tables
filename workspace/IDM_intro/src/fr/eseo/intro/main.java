package fr.eseo.intro;

public class main {
	public static void main(String[] args) {
		Teacher jerome = new Teacher("J. Delatour");
		Teacher fred = new Teacher("F. Jouault");	
		Student cravic = new Student("T. Cravic");
		Student laine = new Student("T. Laine");
		Course idm = new Course("IDM");
		Course os = new Course("OS pour l'embarqué");
		
		fred.addCourse(idm);
		idm.addStudent(cravic);
		os.addTeacher(jerome);
		os.addStudent(laine);
		os.addStudent(cravic);

		System.out.println("---IDM---");
		System.out.println(idm);
		System.out.println("---OS---");
		System.out.println(os);

		System.out.println("---LAINE---");
		System.out.println("Lainé : " + laine.getFormattedCourses());

		System.out.println("---CRAVIC---");
		System.out.println("Cravic : " + cravic.getFormattedCourses());
		System.out.println("---JEROME---");
		System.out.println("Jerome : " + jerome.getFormattedCourses());
		System.out.println("---FRED---");
		System.out.println("Fred : " + fred.getFormattedCourses());
	}
}
