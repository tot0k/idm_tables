package fr.eseo.intro;

import java.util.ArrayList;

public class Course {

	private ArrayList<Student> students;
	private String name;
	private Teacher teacher;
	
	public Course(String name) {
		students = new ArrayList<Student>();
		teacher = null;
		this.name = name;
	}
	
	private synchronized void setTeacher(Teacher teacher) {
		this.teacher = teacher;
	}
	
	public void addStudent(Student student) {
		this.students.add(student);
		if (!student.getCourses().contains(this)) {
			student.addCourse(this);
		}
	}
	
	public void removeStudent(Student student) {
		this.students.remove(student);
		if (student.getCourses().contains(this)) {
			student.removeCourse(this);
		}
	}
	
	public void addTeacher(Teacher teacher) {
		if (this.teacher == null) {
			this.setTeacher(teacher);
			if (!teacher.getCourses().contains(this)) {
				teacher.addCourse(this);
			}
		} else {
			System.err.println("The course already has a teacher.");
		}
	}
	
	
	public ArrayList<Student> getStudents() {
		return students;
	}

	public String getName() {
		return name;
	}

	public Teacher getTeacher() {
		return teacher;
	}

	public String toString() {
		String str = name + "\n";
		str += "Teacher : " + teacher.toString() + "\n";
		str += "Students :" + students.stream().map(e -> e.toString()).reduce(" ", String::concat);
		return str;
	}
}