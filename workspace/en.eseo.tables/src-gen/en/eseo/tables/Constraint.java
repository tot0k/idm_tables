/**
 */
package en.eseo.tables;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Constraint</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link en.eseo.tables.Constraint#getConstraintType <em>Constraint Type</em>}</li>
 *   <li>{@link en.eseo.tables.Constraint#getConcernedHuman <em>Concerned Human</em>}</li>
 *   <li>{@link en.eseo.tables.Constraint#getReferedHuman <em>Refered Human</em>}</li>
 * </ul>
 *
 * @see en.eseo.tables.TablesPackage#getConstraint()
 * @model
 * @generated
 */
public interface Constraint extends EObject {
	/**
	 * Returns the value of the '<em><b>Constraint Type</b></em>' attribute.
	 * The literals are from the enumeration {@link en.eseo.tables.ConstraintType}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Constraint Type</em>' attribute.
	 * @see en.eseo.tables.ConstraintType
	 * @see #setConstraintType(ConstraintType)
	 * @see en.eseo.tables.TablesPackage#getConstraint_ConstraintType()
	 * @model unique="false"
	 * @generated
	 */
	ConstraintType getConstraintType();

	/**
	 * Sets the value of the '{@link en.eseo.tables.Constraint#getConstraintType <em>Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Constraint Type</em>' attribute.
	 * @see en.eseo.tables.ConstraintType
	 * @see #getConstraintType()
	 * @generated
	 */
	void setConstraintType(ConstraintType value);

	/**
	 * Returns the value of the '<em><b>Concerned Human</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link en.eseo.tables.Human#getPlacementConstraints <em>Placement Constraints</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Concerned Human</em>' container reference.
	 * @see #setConcernedHuman(Human)
	 * @see en.eseo.tables.TablesPackage#getConstraint_ConcernedHuman()
	 * @see en.eseo.tables.Human#getPlacementConstraints
	 * @model opposite="placementConstraints" transient="false"
	 * @generated
	 */
	Human getConcernedHuman();

	/**
	 * Sets the value of the '{@link en.eseo.tables.Constraint#getConcernedHuman <em>Concerned Human</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Concerned Human</em>' container reference.
	 * @see #getConcernedHuman()
	 * @generated
	 */
	void setConcernedHuman(Human value);

	/**
	 * Returns the value of the '<em><b>Refered Human</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Refered Human</em>' reference.
	 * @see #setReferedHuman(Human)
	 * @see en.eseo.tables.TablesPackage#getConstraint_ReferedHuman()
	 * @model
	 * @generated
	 */
	Human getReferedHuman();

	/**
	 * Sets the value of the '{@link en.eseo.tables.Constraint#getReferedHuman <em>Refered Human</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Refered Human</em>' reference.
	 * @see #getReferedHuman()
	 * @generated
	 */
	void setReferedHuman(Human value);

} // Constraint
