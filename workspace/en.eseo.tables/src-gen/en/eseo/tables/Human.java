/**
 */
package en.eseo.tables;

import org.eclipse.emf.common.util.EList;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Human</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link en.eseo.tables.Human#getName <em>Name</em>}</li>
 *   <li>{@link en.eseo.tables.Human#getSeat <em>Seat</em>}</li>
 *   <li>{@link en.eseo.tables.Human#getPlacementConstraints <em>Placement Constraints</em>}</li>
 * </ul>
 *
 * @see en.eseo.tables.TablesPackage#getHuman()
 * @model
 * @generated
 */
public interface Human extends Element {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see en.eseo.tables.TablesPackage#getHuman_Name()
	 * @model unique="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link en.eseo.tables.Human#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Seat</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link en.eseo.tables.Seat#getHuman <em>Human</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Seat</em>' reference.
	 * @see #setSeat(Seat)
	 * @see en.eseo.tables.TablesPackage#getHuman_Seat()
	 * @see en.eseo.tables.Seat#getHuman
	 * @model opposite="human"
	 * @generated
	 */
	Seat getSeat();

	/**
	 * Sets the value of the '{@link en.eseo.tables.Human#getSeat <em>Seat</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Seat</em>' reference.
	 * @see #getSeat()
	 * @generated
	 */
	void setSeat(Seat value);

	/**
	 * Returns the value of the '<em><b>Placement Constraints</b></em>' containment reference list.
	 * The list contents are of type {@link en.eseo.tables.Constraint}.
	 * It is bidirectional and its opposite is '{@link en.eseo.tables.Constraint#getConcernedHuman <em>Concerned Human</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Placement Constraints</em>' containment reference list.
	 * @see en.eseo.tables.TablesPackage#getHuman_PlacementConstraints()
	 * @see en.eseo.tables.Constraint#getConcernedHuman
	 * @model opposite="concernedHuman" containment="true"
	 * @generated
	 */
	EList<Constraint> getPlacementConstraints();

} // Human
