/**
 */
package en.eseo.tables.impl;

import en.eseo.tables.Constraint;
import en.eseo.tables.Human;
import en.eseo.tables.Seat;
import en.eseo.tables.TablesPackage;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Human</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link en.eseo.tables.impl.HumanImpl#getName <em>Name</em>}</li>
 *   <li>{@link en.eseo.tables.impl.HumanImpl#getSeat <em>Seat</em>}</li>
 *   <li>{@link en.eseo.tables.impl.HumanImpl#getPlacementConstraints <em>Placement Constraints</em>}</li>
 * </ul>
 *
 * @generated
 */
public class HumanImpl extends ElementImpl implements Human {
	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getSeat() <em>Seat</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSeat()
	 * @generated
	 * @ordered
	 */
	protected Seat seat;

	/**
	 * The cached value of the '{@link #getPlacementConstraints() <em>Placement Constraints</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPlacementConstraints()
	 * @generated
	 * @ordered
	 */
	protected EList<Constraint> placementConstraints;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected HumanImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TablesPackage.Literals.HUMAN;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TablesPackage.HUMAN__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Seat getSeat() {
		if (seat != null && seat.eIsProxy()) {
			InternalEObject oldSeat = (InternalEObject)seat;
			seat = (Seat)eResolveProxy(oldSeat);
			if (seat != oldSeat) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TablesPackage.HUMAN__SEAT, oldSeat, seat));
			}
		}
		return seat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Seat basicGetSeat() {
		return seat;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSeat(Seat newSeat, NotificationChain msgs) {
		Seat oldSeat = seat;
		seat = newSeat;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, TablesPackage.HUMAN__SEAT, oldSeat, newSeat);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setSeat(Seat newSeat) {
		if (newSeat != seat) {
			NotificationChain msgs = null;
			if (seat != null)
				msgs = ((InternalEObject)seat).eInverseRemove(this, TablesPackage.SEAT__HUMAN, Seat.class, msgs);
			if (newSeat != null)
				msgs = ((InternalEObject)newSeat).eInverseAdd(this, TablesPackage.SEAT__HUMAN, Seat.class, msgs);
			msgs = basicSetSeat(newSeat, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TablesPackage.HUMAN__SEAT, newSeat, newSeat));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EList<Constraint> getPlacementConstraints() {
		if (placementConstraints == null) {
			placementConstraints = new EObjectContainmentWithInverseEList<Constraint>(Constraint.class, this, TablesPackage.HUMAN__PLACEMENT_CONSTRAINTS, TablesPackage.CONSTRAINT__CONCERNED_HUMAN);
		}
		return placementConstraints;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TablesPackage.HUMAN__SEAT:
				if (seat != null)
					msgs = ((InternalEObject)seat).eInverseRemove(this, TablesPackage.SEAT__HUMAN, Seat.class, msgs);
				return basicSetSeat((Seat)otherEnd, msgs);
			case TablesPackage.HUMAN__PLACEMENT_CONSTRAINTS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getPlacementConstraints()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TablesPackage.HUMAN__SEAT:
				return basicSetSeat(null, msgs);
			case TablesPackage.HUMAN__PLACEMENT_CONSTRAINTS:
				return ((InternalEList<?>)getPlacementConstraints()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TablesPackage.HUMAN__NAME:
				return getName();
			case TablesPackage.HUMAN__SEAT:
				if (resolve) return getSeat();
				return basicGetSeat();
			case TablesPackage.HUMAN__PLACEMENT_CONSTRAINTS:
				return getPlacementConstraints();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TablesPackage.HUMAN__NAME:
				setName((String)newValue);
				return;
			case TablesPackage.HUMAN__SEAT:
				setSeat((Seat)newValue);
				return;
			case TablesPackage.HUMAN__PLACEMENT_CONSTRAINTS:
				getPlacementConstraints().clear();
				getPlacementConstraints().addAll((Collection<? extends Constraint>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TablesPackage.HUMAN__NAME:
				setName(NAME_EDEFAULT);
				return;
			case TablesPackage.HUMAN__SEAT:
				setSeat((Seat)null);
				return;
			case TablesPackage.HUMAN__PLACEMENT_CONSTRAINTS:
				getPlacementConstraints().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TablesPackage.HUMAN__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case TablesPackage.HUMAN__SEAT:
				return seat != null;
			case TablesPackage.HUMAN__PLACEMENT_CONSTRAINTS:
				return placementConstraints != null && !placementConstraints.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (name: ");
		result.append(name);
		result.append(')');
		return result.toString();
	}

} //HumanImpl
