/**
 */
package en.eseo.tables.impl;

import en.eseo.tables.Constraint;
import en.eseo.tables.ConstraintType;
import en.eseo.tables.Human;
import en.eseo.tables.TablesPackage;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Constraint</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link en.eseo.tables.impl.ConstraintImpl#getConstraintType <em>Constraint Type</em>}</li>
 *   <li>{@link en.eseo.tables.impl.ConstraintImpl#getConcernedHuman <em>Concerned Human</em>}</li>
 *   <li>{@link en.eseo.tables.impl.ConstraintImpl#getReferedHuman <em>Refered Human</em>}</li>
 * </ul>
 *
 * @generated
 */
public class ConstraintImpl extends MinimalEObjectImpl.Container implements Constraint {
	/**
	 * The default value of the '{@link #getConstraintType() <em>Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraintType()
	 * @generated
	 * @ordered
	 */
	protected static final ConstraintType CONSTRAINT_TYPE_EDEFAULT = ConstraintType.FACING;

	/**
	 * The cached value of the '{@link #getConstraintType() <em>Constraint Type</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getConstraintType()
	 * @generated
	 * @ordered
	 */
	protected ConstraintType constraintType = CONSTRAINT_TYPE_EDEFAULT;

	/**
	 * The cached value of the '{@link #getReferedHuman() <em>Refered Human</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getReferedHuman()
	 * @generated
	 * @ordered
	 */
	protected Human referedHuman;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ConstraintImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return TablesPackage.Literals.CONSTRAINT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public ConstraintType getConstraintType() {
		return constraintType;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setConstraintType(ConstraintType newConstraintType) {
		ConstraintType oldConstraintType = constraintType;
		constraintType = newConstraintType == null ? CONSTRAINT_TYPE_EDEFAULT : newConstraintType;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TablesPackage.CONSTRAINT__CONSTRAINT_TYPE, oldConstraintType, constraintType));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Human getConcernedHuman() {
		if (eContainerFeatureID() != TablesPackage.CONSTRAINT__CONCERNED_HUMAN) return null;
		return (Human)eContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Human basicGetConcernedHuman() {
		if (eContainerFeatureID() != TablesPackage.CONSTRAINT__CONCERNED_HUMAN) return null;
		return (Human)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetConcernedHuman(Human newConcernedHuman, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newConcernedHuman, TablesPackage.CONSTRAINT__CONCERNED_HUMAN, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setConcernedHuman(Human newConcernedHuman) {
		if (newConcernedHuman != eInternalContainer() || (eContainerFeatureID() != TablesPackage.CONSTRAINT__CONCERNED_HUMAN && newConcernedHuman != null)) {
			if (EcoreUtil.isAncestor(this, newConcernedHuman))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newConcernedHuman != null)
				msgs = ((InternalEObject)newConcernedHuman).eInverseAdd(this, TablesPackage.HUMAN__PLACEMENT_CONSTRAINTS, Human.class, msgs);
			msgs = basicSetConcernedHuman(newConcernedHuman, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TablesPackage.CONSTRAINT__CONCERNED_HUMAN, newConcernedHuman, newConcernedHuman));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Human getReferedHuman() {
		if (referedHuman != null && referedHuman.eIsProxy()) {
			InternalEObject oldReferedHuman = (InternalEObject)referedHuman;
			referedHuman = (Human)eResolveProxy(oldReferedHuman);
			if (referedHuman != oldReferedHuman) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, TablesPackage.CONSTRAINT__REFERED_HUMAN, oldReferedHuman, referedHuman));
			}
		}
		return referedHuman;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Human basicGetReferedHuman() {
		return referedHuman;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void setReferedHuman(Human newReferedHuman) {
		Human oldReferedHuman = referedHuman;
		referedHuman = newReferedHuman;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, TablesPackage.CONSTRAINT__REFERED_HUMAN, oldReferedHuman, referedHuman));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TablesPackage.CONSTRAINT__CONCERNED_HUMAN:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetConcernedHuman((Human)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case TablesPackage.CONSTRAINT__CONCERNED_HUMAN:
				return basicSetConcernedHuman(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case TablesPackage.CONSTRAINT__CONCERNED_HUMAN:
				return eInternalContainer().eInverseRemove(this, TablesPackage.HUMAN__PLACEMENT_CONSTRAINTS, Human.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case TablesPackage.CONSTRAINT__CONSTRAINT_TYPE:
				return getConstraintType();
			case TablesPackage.CONSTRAINT__CONCERNED_HUMAN:
				if (resolve) return getConcernedHuman();
				return basicGetConcernedHuman();
			case TablesPackage.CONSTRAINT__REFERED_HUMAN:
				if (resolve) return getReferedHuman();
				return basicGetReferedHuman();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case TablesPackage.CONSTRAINT__CONSTRAINT_TYPE:
				setConstraintType((ConstraintType)newValue);
				return;
			case TablesPackage.CONSTRAINT__CONCERNED_HUMAN:
				setConcernedHuman((Human)newValue);
				return;
			case TablesPackage.CONSTRAINT__REFERED_HUMAN:
				setReferedHuman((Human)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case TablesPackage.CONSTRAINT__CONSTRAINT_TYPE:
				setConstraintType(CONSTRAINT_TYPE_EDEFAULT);
				return;
			case TablesPackage.CONSTRAINT__CONCERNED_HUMAN:
				setConcernedHuman((Human)null);
				return;
			case TablesPackage.CONSTRAINT__REFERED_HUMAN:
				setReferedHuman((Human)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case TablesPackage.CONSTRAINT__CONSTRAINT_TYPE:
				return constraintType != CONSTRAINT_TYPE_EDEFAULT;
			case TablesPackage.CONSTRAINT__CONCERNED_HUMAN:
				return basicGetConcernedHuman() != null;
			case TablesPackage.CONSTRAINT__REFERED_HUMAN:
				return referedHuman != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy()) return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (constraintType: ");
		result.append(constraintType);
		result.append(')');
		return result.toString();
	}

} //ConstraintImpl
