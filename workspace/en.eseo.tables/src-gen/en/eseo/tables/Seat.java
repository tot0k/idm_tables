/**
 */
package en.eseo.tables;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Seat</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link en.eseo.tables.Seat#getIndex <em>Index</em>}</li>
 *   <li>{@link en.eseo.tables.Seat#getTable <em>Table</em>}</li>
 *   <li>{@link en.eseo.tables.Seat#getHuman <em>Human</em>}</li>
 * </ul>
 *
 * @see en.eseo.tables.TablesPackage#getSeat()
 * @model
 * @generated
 */
public interface Seat extends Element {
	/**
	 * Returns the value of the '<em><b>Index</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Index</em>' attribute.
	 * @see #setIndex(int)
	 * @see en.eseo.tables.TablesPackage#getSeat_Index()
	 * @model unique="false"
	 * @generated
	 */
	int getIndex();

	/**
	 * Sets the value of the '{@link en.eseo.tables.Seat#getIndex <em>Index</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Index</em>' attribute.
	 * @see #getIndex()
	 * @generated
	 */
	void setIndex(int value);

	/**
	 * Returns the value of the '<em><b>Table</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link en.eseo.tables.Table#getSeats <em>Seats</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Table</em>' container reference.
	 * @see #setTable(Table)
	 * @see en.eseo.tables.TablesPackage#getSeat_Table()
	 * @see en.eseo.tables.Table#getSeats
	 * @model opposite="seats" transient="false"
	 * @generated
	 */
	Table getTable();

	/**
	 * Sets the value of the '{@link en.eseo.tables.Seat#getTable <em>Table</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Table</em>' container reference.
	 * @see #getTable()
	 * @generated
	 */
	void setTable(Table value);

	/**
	 * Returns the value of the '<em><b>Human</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link en.eseo.tables.Human#getSeat <em>Seat</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Human</em>' reference.
	 * @see #setHuman(Human)
	 * @see en.eseo.tables.TablesPackage#getSeat_Human()
	 * @see en.eseo.tables.Human#getSeat
	 * @model opposite="seat"
	 * @generated
	 */
	Human getHuman();

	/**
	 * Sets the value of the '{@link en.eseo.tables.Seat#getHuman <em>Human</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Human</em>' reference.
	 * @see #getHuman()
	 * @generated
	 */
	void setHuman(Human value);

} // Seat
