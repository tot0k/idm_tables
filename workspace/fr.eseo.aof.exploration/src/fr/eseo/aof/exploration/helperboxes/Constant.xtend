package fr.eseo.aof.exploration.helperboxes

import java.util.Collections
import org.eclipse.papyrus.aof.core.IObservable
import org.eclipse.papyrus.aof.core.IObserver

interface Constant<E> extends IObservable<E> {	
	override addObserver(IObserver<E> observer) {
	}
	
	override getObservers() {
		Collections.emptyList
	}

	// This interface should not be used in a context where the isObserved method might be called
	override isObserved() {
		throw new UnsupportedOperationException("Cannot meaningfully answer")
	}
	
	override removeObserver(IObserver<E> observer) {
	}
	
}