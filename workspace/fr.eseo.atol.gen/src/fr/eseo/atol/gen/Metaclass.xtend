package fr.eseo.atol.gen

import org.eclipse.papyrus.aof.core.IMetaClass

interface Metaclass<C> extends IMetaClass<C> {
	override isInstance(Object object) {
		throw new UnsupportedOperationException
	}

	override isSubTypeOf(IMetaClass<?> that) {
		throw new UnsupportedOperationException
	}

	override getDefaultInstance() {
		throw new UnsupportedOperationException
	}

	override setDefaultInstance(C defaultInstance) {
		throw new UnsupportedOperationException
	}

	override <E> getPropertyAccessor(Object property) {
		throw new UnsupportedOperationException
	}
}
