package en.eseo.tables2jfx

import fr.eseo.atol.gen.ATOLGen
import fr.eseo.atol.gen.ATOLGen.Metamodel
import fr.eseo.atol.gen.plugin.constraints.common.Constraints

@ATOLGen(transformation="src/en/eseo/tables2jfx/tables2jfx.atl", metamodels = #[
	@Metamodel(name = "Tables", impl = Tables),
	@Metamodel(name = "JFX", impl = JFX),
	@Metamodel(name = "Constraints", impl = Constraints)], extensions = #[Constraints])
class tables2jfx {
	
}