package en.eseo.tables2jfx

import en.eseo.tables.Seat
import en.eseo.tables.TablesPackage
import fr.eseo.aof.xtend.utils.AOFAccessors

import static fr.eseo.atol.gen.MetamodelUtils.*

@AOFAccessors(TablesPackage)
class Tables {
	public val __index = <Seat, Integer>oneDefault(0)[	
		_index
	]
}