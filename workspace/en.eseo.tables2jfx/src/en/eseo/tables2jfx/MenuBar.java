/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package en.eseo.tables2jfx;

import java.io.File;
import java.util.Optional;

import org.eclipse.emf.common.util.EList;
import org.eclipse.papyrus.aof.core.IBox;

import en.eseo.tables.Element;
import en.eseo.tables.Human;
import en.eseo.tables.Seat;
import en.eseo.tables.Table;
import en.eseo.tables2jfx.tables2jfx.TargetTupleTable;
import fr.eseo.atol.gen.AbstractRule;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.TextInputDialog;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.shape.Circle;
import javafx.scene.text.Text;
import javafx.stage.FileChooser;
import javafx.util.Callback;


/**
 *
 * @author Thomas LAINÉ, Thomas CRAVIC
 */
public class MenuBar {
    
    
    private static final int BTN_HEIGHT = 30;
    private static final int BTN_WIDTH = 100;
    private static final int SPACING = 15;
    private final VBox toolBox;
    private VBox toolbox_guest_menu;
    private VBox toolbox_table_menu;
    private final RunTables2JFX master;
    
    
    protected final ComboBox<Human> humanSelector; 
    private VBox contextMenu;
    
    
    public void changeMenu(IBox<Element> element) {
    	String menuType = element.get(0).getClass().getSimpleName();
    	System.out.println(menuType);
    	
    	toolBox.getChildren().remove(contextMenu);
    	switch (menuType) {
	    	case "HumanImpl":
	    		contextMenu = createGuestMenu((Human) element);
	    		break;
	    	case "TableImpl":
	    		contextMenu = createTableMenu(element.select(Table.class));
	    		break;
	    	case "SeatImpl":
	    		System.err.println("Seats context menu not implemented yet");
	    		break;
    		default:
    			System.err.println(menuType + " context menu not implemented yet");
    			break;
    	}
    	if (contextMenu != null) {
    		toolBox.getChildren().add(contextMenu);
    	}
    }
    
    
    public MenuBar(BorderPane bdp, RunTables2JFX master) {

    	this.master = master;
    	toolBox = new VBox();
        toolBox.setStyle("-fx-background-color: #D0D0D0;");
        toolBox.setSpacing(SPACING);
        toolBox.setMinSize(master.toolboxWidth, master.paneHeight);
        bdp.setRight(toolBox);
        humanSelector = new ComboBox();
        humanSelector.setMaxSize(2.5 * BTN_WIDTH, BTN_HEIGHT);
        humanSelector.setMinSize(2 * BTN_WIDTH, BTN_HEIGHT);
        //humanSelector.setButtonCell(new HumanSelectorCell());
        Callback<ListView<Human>, ListCell<Human>> factory = lv -> new ListCell<Human>() {

            @Override
            protected void updateItem(Human item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? "" : item.getName());
                if (!empty) {
                	//changeMenu(humanSelector.getValue());
                	System.out.println(item);
                }
            }
        };
        
        humanSelector.setCellFactory(factory);
        humanSelector.setButtonCell(factory.call(null));

        // The management part of the toolbox.
        VBox toolBox_management = new VBox();
        toolBox_management.setSpacing(SPACING);
        toolBox_management.setStyle("-fx-padding: 10;" + 
                      "-fx-border-style: solid inside;" + 
                      "-fx-border-width: 2;" +
                      "-fx-border-insets: 5;" + 
                      "-fx-border-radius: 5;" + 
                      "-fx-border-color: black;");
        toolBox.getChildren().add(toolBox_management);
        
        
        // HBox des boutons addTable et addGuest.
        HBox adds_hbox = new HBox();
        // Espace entre deux enfants de adds_hbox
        adds_hbox.setSpacing(SPACING);
        // Décalage de l'adds_hbox de 15 pixels vers la droite.
        adds_hbox.setTranslateX(SPACING);
        // Ahiyt de l'adds_hbox à la toolbox.
        toolBox_management.getChildren().add(adds_hbox);
        
        // Bouton add table
        Button btn_add_table = new Button();
        // Changement du texte du bouton btn_add_table
        btn_add_table.setText("Add table");
        
        // Modification la taille du bouton.
        btn_add_table.setPrefSize(BTN_WIDTH, BTN_HEIGHT);
        btn_add_table.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                
            	TextInputDialog dialog = new TextInputDialog("4");
            	dialog.setTitle("Create Table");
            	dialog.setContentText("Please enter the number of seats :");

            	// Traditional way to get the response value.
            	Optional<String> result = dialog.showAndWait();
            	if (result.isPresent()){
            		try {
            			master.createTable(Integer.parseInt(result.get()));
            		} catch (NumberFormatException ex) {
            			System.out.println("The value is not a number.");
            		}
            	}
            }
        });
        adds_hbox.getChildren().add(btn_add_table);
        
        // Bouton add guest
        Button btn_add_guest = new Button();
        btn_add_guest.setText("Add guest");
        btn_add_guest.setPrefSize(BTN_WIDTH, BTN_HEIGHT);
        btn_add_guest.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
            	TextInputDialog dialog = new TextInputDialog("Guest");
            	dialog.setTitle("Add guest");
            	dialog.setContentText("Please enter guest name:");

            	// Traditional way to get the response value.
            	Optional<String> result = dialog.showAndWait();
            	if (result.isPresent()){
            		master.createGuest(result.get());
            		humanSelector.getItems().setAll(master.rootClass.get(0).getHumans());
            	}
            }
        });
        adds_hbox.getChildren().add(btn_add_guest);
        
        // Ajout du vbox pour l'autoReload.
        HBox autoReload_hbox = new HBox();
        autoReload_hbox.setSpacing(SPACING);
        autoReload_hbox.setTranslateX(2*SPACING);
        toolBox_management.getChildren().add(autoReload_hbox);
        
        // Bouton radio de l'autoReload
        /*RadioButton rb_autoReload = new RadioButton();
        autoReload_hbox.getChildren().add(rb_autoReload);
        
        // Text autoReload
        Text txt_autoReload = new Text();
        txt_autoReload.setText("Auto-reload ?");
        autoReload_hbox.getChildren().add(txt_autoReload);
        */
        // Bouton Force Reload
        Button btn_forceReload = new Button();
        btn_forceReload.setText("Force Reload");
        btn_forceReload.setTranslateX(2*SPACING);
        btn_forceReload.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Force the reload !");
                master.doTransformation();
            }
        });
        toolBox_management.getChildren().add(btn_forceReload);
        
        // HBox des boutons addTable et addGuest.
        HBox load_save_hbox = new HBox();
        // Espace entre deux enfants de adds_hbox
        load_save_hbox.setSpacing(SPACING);
        // Décalage de l'adds_hbox de 15 pixels vers la droite.
        load_save_hbox.setTranslateX(SPACING);
        // Ahiyt de l'adds_hbox à la toolbox.
        toolBox_management.getChildren().add(load_save_hbox);
        
        // Bouton add table
        Button btn_load = new Button();
        // Changement du texte du bouton btn_add_table
        btn_load.setText("Load");
        // Modification la taille du bouton.
        btn_load.setPrefSize(BTN_WIDTH, BTN_HEIGHT);
        btn_load.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Load a table plan");
                final FileChooser fileChooser = new FileChooser();
                File file = fileChooser.showOpenDialog(master.stage);
                if (file != null) {
                    master.loadFile(file.getAbsolutePath());
                }
            }
        });
        load_save_hbox.getChildren().add(btn_load);
        
        // Bouton add guest
        Button btn_save = new Button();
        btn_save.setText("Save");
        btn_save.setPrefSize(BTN_WIDTH, BTN_HEIGHT);
        btn_save.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Save the table plan.");
                final FileChooser fileChooser = new FileChooser();
                FileChooser.ExtensionFilter extFilter = new FileChooser.ExtensionFilter("XMI files (*.xmi)", "*.xmi");
                fileChooser.getExtensionFilters().add(extFilter);
                
				File file = fileChooser.showSaveDialog(master.stage);
                
                if (file != null) {
                	String path = file.getAbsolutePath();
                	if (!path.endsWith(".xmi")) {
                		path = path + ".xmi";
                	}
                    master.saveFile(path);
                }
            }
        });
        load_save_hbox.getChildren().add(btn_save);
        
        // The Guest List part of the toolbox.
        VBox toolbox_guestList = new VBox();
        humanSelector.getItems().setAll(master.rootClass.get(0).getHumans());

        toolbox_guestList.setSpacing(SPACING);
        toolbox_guestList.setStyle("-fx-padding: 10;" + 
                      "-fx-border-style: solid inside;" + 
                      "-fx-border-width: 2;" +
                      "-fx-border-insets: 5;" + 
                      "-fx-border-radius: 5;" + 
                      "-fx-border-color: black;");
        toolBox.getChildren().add(toolbox_guestList);
        
        // GUEST LIST Text
        Text txt_guest_list = new Text();
        txt_guest_list.setText("GUEST LIST");
        toolbox_guestList.getChildren().addAll(txt_guest_list, humanSelector);
        
    }
    
    private VBox createGuestMenu(Human human) {
    	// The Gest part of the toolbox.
        toolbox_guest_menu = new VBox();
        toolbox_guest_menu.setSpacing(SPACING);
        toolbox_guest_menu.setStyle("-fx-padding: 10;" + 
                      "-fx-border-style: solid inside;" + 
                      "-fx-border-width: 2;" +
                      "-fx-border-insets: 5;" + 
                      "-fx-border-radius: 5;" + 
                      "-fx-border-color: black;");
        
        // The guest menu  Text
        Text txt_guest_menu = new Text();
        txt_guest_menu.setText("GUEST MENU");
        toolbox_guest_menu.getChildren().add(txt_guest_menu);
        
        // Name hbox
        HBox guest_name_hbox = new HBox();
        guest_name_hbox.setSpacing(10);
        toolbox_guest_menu.getChildren().add(guest_name_hbox);
        // Name text
        Text guest_name_txt = new Text();
        guest_name_txt.setText("Name");
        guest_name_hbox.getChildren().add(guest_name_txt);
        // Name text field
        TextField guest_name_txtField = new TextField ();
        guest_name_hbox.getChildren().add(guest_name_txtField);
        
        //toolBox.getChildren().add(toolbox_guest_menu);
        return toolbox_guest_menu;
    }
    
//    private Double parseDouble(String str, Double previous, TextField textField) {
//    	double value = 0.0;
//    	try {
//    		value = Double.parseDouble(str);
//    	} catch (Exception e) {
//    		System.err.println("TODO : Handle the error better");
//    		value = previous;
//    		textField.setText(Double.toString(value));
//    	}
//    	return value;
//    }
    
    private VBox createTableMenu(IBox<Table> element) {
    	// The Table menu part of the toolbox.
        toolbox_table_menu = new VBox();
        toolbox_table_menu.setSpacing(SPACING);
        toolbox_table_menu.setStyle("-fx-padding: 10;" + 
                      "-fx-border-style: solid inside;" + 
                      "-fx-border-width: 2;" +
                      "-fx-border-insets: 5;" + 
                      "-fx-border-radius: 5;" + 
                      "-fx-border-color: black;");
        
        // The table menu  Text
        Text txt_table_menu = new Text();
        txt_table_menu.setText("TABLE MENU");
        toolbox_table_menu.getChildren().add(txt_table_menu);
        
        // Name hbox
        HBox table_name_hbox = new HBox();
        table_name_hbox.setSpacing(10);
        toolbox_table_menu.getChildren().add(table_name_hbox);
        // Name text
        Text table_name_txt = new Text();
        table_name_txt.setText("Name");
        table_name_hbox.getChildren().add(table_name_txt);
        // Name text field
        TextField table_name_txtField = new TextField ();
        table_name_txtField.setText(element.get(0).getName());
        table_name_hbox.getChildren().add(table_name_txtField);
        table_name_txtField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable,
                    String oldValue, String newValue) {

            	element.get(0).setName(newValue);
            }
        });
        
        // X
        HBox table_x_hbox = new HBox();
        table_x_hbox.setSpacing(10);
        toolbox_table_menu.getChildren().add(table_x_hbox);
        // X text
        Text table_x_txt = new Text();
        table_x_txt.setText("X");
        table_x_hbox.getChildren().add(table_x_txt);
        // X text field
        TextField table_x_txtField = new TextField();
        table_x_txtField.textProperty().addListener(new ChangeListener<String>() {
            @Override
            public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
                if (!newValue.matches("\\d{0,7}([\\.]\\d{0,4})?")) {
                	table_x_txtField.setText(oldValue);
                } else {
                	TargetTupleTable target = master.transfo.Table(element.get(0));
                	master.c2cas.suggestValue(target.c.centerXProperty(), Double.parseDouble(newValue));
                }
            }
        });

        AbstractRule.operator_spaceship(
        		table_x_txtField.textProperty(),
        		new Tables().x(element).collect(
        				it -> it == null ? "" : Integer.toString((int)Math.round(it)),
        						it -> it == null ? 0 : Double.valueOf(Integer.parseInt(it))
        						)
        		);
        
        table_x_hbox.getChildren().add(table_x_txtField);
        
        // Y
        HBox table_y_hbox = new HBox();
        table_y_hbox.setSpacing(10);
        toolbox_table_menu.getChildren().add(table_y_hbox);
        // Y text
        Text table_y_text = new Text();
        table_y_text.setText("Y");
        table_y_hbox.getChildren().add(table_y_text);
        // Y text field
        TextField table_y_txtField = new TextField();
        AbstractRule.operator_spaceship(
        		table_y_txtField.textProperty(),
        		new Tables().y(element).collect(
        				it -> it == null ? "" : Integer.toString((int)Math.round(it)),
        						it -> it == null ? 0 : Double.valueOf(Integer.parseInt(it))
        						)
        		);
        table_y_hbox.getChildren().add(table_y_txtField);

        
        // Diameter
        HBox table_radius_hbox = new HBox();
        table_radius_hbox.setSpacing(10);
        toolbox_table_menu.getChildren().add(table_radius_hbox);
        // Diameter text
        Text table_radius_text = new Text();
        table_radius_text.setText("Radius");
        table_radius_hbox.getChildren().add(table_radius_text);
        // Diameter text field
        Slider slider_radius = new Slider();
        table_radius_hbox.getChildren().add(slider_radius);
        
        // The minimum value.
        slider_radius.setMin(40);
         
        // The maximum value.
        slider_radius.setMax(200);
         
        // Current value
        slider_radius.setValue(element.get(0).getRadius());
         
        slider_radius.setShowTickLabels(true);
        slider_radius.setShowTickMarks(true);
        slider_radius.setBlockIncrement(10);
        
        
        slider_radius.valueProperty().addListener(new ChangeListener<Number>() {
        	 
            @Override
            public void changed(ObservableValue<? extends Number> observable,
                  Number oldValue, Number newValue) {
            	element.get(0).setRadius((Double) newValue);
            }
         });
        
        // Seats
        HBox table_seats_hbox = new HBox();
        table_seats_hbox.setSpacing(10);
        toolbox_table_menu.getChildren().add(table_seats_hbox);
        // Seats text
        Text table_seats_text = new Text();
        table_seats_text.setText("Seats");
        table_seats_hbox.getChildren().add(table_seats_text);
        // Seats text field
        ComboBox<Seat> seatSelector = new ComboBox();
        seatSelector.setMaxSize(2.5 * BTN_WIDTH, BTN_HEIGHT);
        seatSelector.setMinSize(2 * BTN_WIDTH, BTN_HEIGHT);
        //humanSelector.setButtonCell(new HumanSelectorCell());
        Callback<ListView<Seat>, ListCell<Seat>> factory = lv -> new ListCell<Seat>() {

            @Override
            protected void updateItem(Seat item, boolean empty) {
                super.updateItem(item, empty);
                setText(empty ? "" : Integer.toString(item.getIndex())); // TODO : change index
                if (!empty) {
                	//changeMenu(humanSelector.getValue());
                	System.out.println(item);
                }
            }
        };
        
        seatSelector.setCellFactory(factory);
        seatSelector.setButtonCell(factory.call(null));
        seatSelector.getItems().setAll(element.get(0).getSeats());
        table_seats_hbox.getChildren().add(seatSelector);
        
        // Color
        /*HBox table_color_hbox = new HBox();
        table_color_hbox.setSpacing(10);
        toolbox_table_menu.getChildren().add(table_color_hbox);
        // Color text
        Text table_color_text = new Text();
        table_color_text.setText("Color");
        table_color_hbox.getChildren().add(table_color_text);
        // Color text field
        TextField table_color_txtField = new TextField();
        table_color_txtField.setText(element.get(0).getColor());
        table_color_hbox.getChildren().add(table_color_txtField);*/
        
        return toolbox_table_menu;
        //toolBox.getChildren().add(toolbox_table_menu);
    }
    
}
