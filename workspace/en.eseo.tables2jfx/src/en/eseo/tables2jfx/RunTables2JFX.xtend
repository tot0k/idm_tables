package en.eseo.tables2jfx

import en.eseo.constraints.tables2Constraints
import en.eseo.tables.ConstraintType
import en.eseo.tables.Element
import en.eseo.tables.Human
import en.eseo.tables.Root
import en.eseo.tables.Seat
import en.eseo.tables.Table
import en.eseo.tables.TablesFactory
import fr.eseo.atlc.constraints.Constraint
import fr.eseo.atlc.constraints.ConstraintGroup
import fr.eseo.atol.gen.AbstractRule
import fr.eseo.atol.gen.plugin.constraints.solvers.Constraints2Cassowary
import fr.eseo.atol.gen.plugin.constraints.solvers.Constraints2Choco
import java.util.Collections
import java.util.HashMap
import java.util.Map
import java.util.Random
import javafx.application.Application
import javafx.scene.Node
import javafx.scene.Scene
import javafx.scene.control.TextInputDialog
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseButton
import javafx.scene.layout.BorderPane
import javafx.scene.layout.Pane
import javafx.scene.shape.Circle
import javafx.scene.shape.Rectangle
import javafx.scene.text.Text
import javafx.stage.Stage
import org.eclipse.emf.common.util.URI
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl
import org.eclipse.papyrus.aof.core.AOFFactory
import org.eclipse.papyrus.aof.core.IBox

import static extension fr.eseo.aof.exploration.OCLByEquivalence.closure
import static extension fr.eseo.atol.gen.JFXUtils.*
import org.eclipse.emf.ecore.util.EcoreUtil

class RunTables2JFX extends Application {
	
	public var IBox<Root> rootClass
	val left = new Pane
	val right = new Pane
	val Map<Node, Element> target2Source = new HashMap
	public val transfo = new tables2jfx
	private var MenuBar menuBar;
	public val toolboxWidth = 275
	public val paneWidth = 1600 - toolboxWidth
	public val paneHeight = 900
	public var Stage stage
	val IBox<Element> selectedElement = AOFFactory.INSTANCE.createOption
	
	public var Constraints2Cassowary c2cas
	var int nbOfTables = 0
	

	override start(Stage primaryStage) throws Exception {
		// Déclaration de la transformation
		
		extension val tables = transfo.TablesMM
		extension val jfx = transfo.JFXMM

		// Creating the different elements
		rootClass = AOFFactory.INSTANCE.createOne(TablesFactory.eINSTANCE.createRoot)
		
		
//		table._x.inspect("x: ")

		createTable(13)
		createTable(5)
		createTable(3)
		createTable(8)
		
		initTransfo()
		
		val f = rootClass.collect[transfo.Root(it).f]
//		val f = transfo.Root(rootClass.get(0)).f
		val figures = f.concat(f.children.closure[
			it._children
		])
		
		val constraints = figures.collectMutable[
			it?._constraints ?: AbstractRule.emptySequence
		]//.inspect("constraints: ")
		val nodes = figures.collectMutable[
			it?._nodes ?: AbstractRule.emptySequence
		]//.inspect("nodes: ")

		c2cas = new Constraints2Cassowary(jfx, tables, Constraints2Cassowary.geometricAbstraction)

		
		c2cas.apply(constraints.collect[it])
		c2cas.solve
		
		//rootClass.tables.forEach[ t | ]
		

		nodes.select[_movable(it).get].collect[
			onPress
			onDrag
			if(it instanceof Circle) {
				val s = target2Source.get(it) as Table
				//println((s as Table).x + " " + (s as Table).y)
				c2cas.suggestValue(centerXProperty, (s as Table).x)
				c2cas.suggestValue(centerYProperty, (s as Table).y)
				
				s._x.bind(it._centerX.asOne(0.0))
				s._y.bind(it._centerY.asOne(0.0))
			}
			null
		]

		nodes.select[_editable(it).get].collect[
			onDoubleClick
			null
		]

		left.children.toBox.bind(nodes)
		
		showEditor(primaryStage)
	}
	
	
	def void initTransfo() {
		transfo.Table.registerCustomTrace = [s, t |
			target2Source.put(t.c, s.s)
			target2Source.put(t.txt, s.s)
			
		]
		transfo.Seat.registerCustomTrace = [s, t |
			target2Source.put(t.r, s.s)
			target2Source.put(t.l, s.s)
			target2Source.put(t.txt, s.s)
		]
	}
	
	def void addSeat(Table table, int id) {
		val seat = TablesFactory.eINSTANCE.createSeat
		seat.index = id
		table.seats.add(seat)
	}
	
	def void createTable(int nbSeats) {
		val table = TablesFactory.eINSTANCE.createTable
		val rand = new Random
		var radius = 80
		
		nbOfTables = nbOfTables + 1
		table.x = rand.nextInt(paneWidth - 2 * (radius + 60)) + radius + 60
		table.y = rand.nextInt(paneHeight - 2 * (radius + 60)) + radius + 60
		table.radius = radius
		table.name = "Table " + nbOfTables
		
		for (i : 1 ..< nbSeats + 1) {
			addSeat(table, i)
		}
		rootClass.get(0).tables.add(table)
		selectedElement.set(0, table)
	}
	
	def void createGuest(String name) {
		val gest = TablesFactory.eINSTANCE.createHuman
		gest.name = name
		rootClass.get(0).humans.add(gest)
	}
	
	def void addConstraint(String human1, String human2, String constraintName) {
		val constraint = TablesFactory.eINSTANCE.createConstraint
		
		switch (constraintName) {
			case "FACING" : constraint.constraintType = ConstraintType.FACING
			case "LEFT_OF" : constraint.constraintType = ConstraintType.LEFT_OF
			case "RIGHT_OF" : constraint.constraintType = ConstraintType.RIGHT_OF
			case "NEXT_TO" : constraint.constraintType = ConstraintType.NEXT_TO
			case "SAME_TABLE" : constraint.constraintType = ConstraintType.SAME_TABLE
		}
		
		//TODO: watch out
		rootClass.get(0).humans.forEach[ h | if(h.name == human1) {
			constraint.concernedHuman = h
			h.placementConstraints.add(constraint)
		} else if(h.name == human2) {
			constraint.referedHuman = h
		}]
	}
	
	def void loadFile(String path) {
		println("Loading file from " + path + ".")
		val rs = new ResourceSetImpl
		rs.resourceFactoryRegistry.extensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl)
		val r = rs.getResource(URI.createFileURI(path), true)
		transfoConstraints = null	// the business rules solver will be recreated when necessary
		rootClass.set(0, r.contents.get(0) as Root)
		
	}
	
	def void saveFile(String path) {
		println("Saving file to " + path + ".")
		val rs = new ResourceSetImpl
		rs.resourceFactoryRegistry.extensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl)
		val r = rs.createResource(URI.createFileURI(path))
		r.contents.add(rootClass.get(0))
		r.save(Collections.emptyMap)
	}
	
	def static void main(String[] args) {
		launch(args)
	}

	var tables2Constraints transfoConstraints = null
	var Constraints2Choco c2cho = null
	def showEditor(Stage theStage) {
		stage = theStage
		val BorderPane root = new BorderPane()
		menuBar = new MenuBar(root, this)
		
		left.prefWidth = paneWidth
		right.prefWidth = toolboxWidth
		root.children.addAll(left)
		root.children.addAll(right)
		
		val scene = new Scene(root, paneWidth + toolboxWidth, paneHeight);

		left.style = "-fx-border-color: #000000;"
		right.style = "-fx-border-color: #000000;"
		stage.setScene(scene);
		
		scene.addEventFilter(KeyEvent.KEY_PRESSED, [KeyEvent t |
			switch t.getCode {
				case ESCAPE: {
					stage.close
				}
				case T: {
					createTable(5)
				}
				case DELETE: {
					print("Delete element")
					removeElement
				}
				case S: {
					addSeat((selectedElement as Table), (selectedElement as Table).seats.size + 1)
				}
				case L: {
					loadFile("test2.xmi")
				}
				case A: {
					doTransformation()
				}
				default: {
					return
				}
			}
		]);
		
		stage.setTitle("Plan de tables - CRAVIC / LAINÉ");
		stage.show();
		stage.onCloseRequest = [
			println("Close requested")
		]
	}
	
	def doTransformation() {
		if(transfoConstraints === null) {
						transfoConstraints = new tables2Constraints
						
						val r = rootClass.get(0)
						transfoConstraints.refineRoot(r)
						transfoConstraints.refine(r)
						val allConstraints =
									AOFFactory.INSTANCE.<EObject>createOne(r).concat(transfoConstraints.allContents(r, null)).collect[
										transfoConstraints.trace.get(it)
									].select[it !== null].collectMutable[
										if(it === null) {
											return AbstractRule.emptySequence
										}
										AOFFactory.INSTANCE.createSequence(
											values.filter(ConstraintGroup) as ConstraintGroup[]
										)
									]
						allConstraints.forEach[println(it)]
						c2cho = new Constraints2Choco(transfo.JFXMM, transfoConstraints.TablesMM)
						c2cho.defaultLowerBound = 0
						c2cho.defaultUpperBound = 26
						c2cho.apply(allConstraints.select[solver == "choco"].collect[it as Constraint])
					}
					c2cho.debug
					c2cho.solve
					println("************************************")
					c2cho.debug
					
					var rs = new ResourceSetImpl
					rs.resourceFactoryRegistry.extensionToFactoryMap.put("xmi", new XMIResourceFactoryImpl)
					var r = rs.createResource(URI.createFileURI("test-output.xmi"))
					r.contents.add(EcoreUtil.copy(rootClass.get(0)))
					r.save(System.out, Collections.emptyMap)
	}
	
	def removeElement() {
		if (selectedElement !== null) {
			var elem = selectedElement.get(0)
			if (elem instanceof Table) {
				var table = elem as Table
				println("Removing table " + table.name + ".")
				rootClass.get(0).tables.remove(table)
			
			} else if( elem instanceof Seat) {
				
				var seat = elem as Seat
				var table = seat.table //var table = rootClass.tables.get(rootClass.tables.indexOf(seat.table))
				println("Removing seat " + seat.name + ".")
				table.seats.remove(seat)
				
			}
		}
	}

	static dispatch def xProp(Rectangle it) {
		xProperty
	}

	static dispatch def xProp(Text it) {
		xProperty
	}

	static dispatch def xProp(Circle it) {
		centerXProperty
	}

	static dispatch def yProp(Rectangle it) {
		yProperty
	}

	static dispatch def yProp(Text it) {
		yProperty
	}

	static dispatch def yProp(Circle it) {
		centerYProperty
	}

	var dx = 0.0
	var dy = 0.0
	
	def onPress(Node it) {
		onMousePressed = [e |
			val t = e.target
			switch t {
				Text,
				Rectangle,
				Circle: {
					dx = e.x - t.xProp.get
					dy = e.y - t.yProp.get
					e.consume
				}
			}
			val s = target2Source.get(t)
			if(s !== null) {
				println("Selected element : " + s.getName)
				selectedElement.set(0, s)
				if (selectedElement !== null) {
					menuBar.changeMenu(selectedElement)
				}
			}
		]
	}
	
	def String getName(Element element) {
		if (element instanceof Table) {
			return (element as Table).name
			
		} else if( element instanceof Human) {
			return (element as Human).name
			
		} else if (element instanceof Seat) {
			return element.table.name + " - seat " + element.index
		}
	}

	def onDrag(Node it) {
		onMouseDragged = [e |
			val t = e.target
			switch t {
				Text,
				Rectangle,
				Circle:
					switch (e.button) {
						case MouseButton.PRIMARY: {
							var Constraints2Cassowary solver =
									c2cas

							if (solver.hasVariable(t.xProp) && solver.hasVariable(t.yProp)) {
								solver.suggestValue(t.xProp, e.x - dx)
								solver.suggestValue(t.yProp, e.y - dy)
							}

							e.consume
						}
						default: {}
					}
				}
		]
	}

	def onDoubleClick(Node it) {
		if (it instanceof Text) {
			onMouseClicked = [e |
				if (e.clickCount == 2) {
					val dialog = new TextInputDialog(text)
					dialog.title = "Change name"

					val newText = dialog.showAndWait
					if (newText.present && !newText.get.empty) {
						text = newText.get
					}
				}
			]
		}
	}
}