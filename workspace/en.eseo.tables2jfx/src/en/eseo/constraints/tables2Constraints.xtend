package en.eseo.constraints

import en.eseo.tables2jfx.Tables
import fr.eseo.atol.gen.ATOLGen
import fr.eseo.atol.gen.ATOLGen.Metamodel
import fr.eseo.atol.gen.plugin.constraints.common.Constraints
import java.util.HashMap
import java.util.Map
import fr.eseo.aof.extensions.AOFExtensions
 
@ATOLGen(transformation="src/en/eseo/constraints/tablesConstraints.atl", metamodels = #[
	@Metamodel(name = "Tables", impl = Tables),
	@Metamodel(name = "Constraints", impl = Constraints)], extensions = #[Constraints])
class tables2Constraints implements AOFExtensions {
	public val trace = new HashMap<Object, Map<String, Object>>
}