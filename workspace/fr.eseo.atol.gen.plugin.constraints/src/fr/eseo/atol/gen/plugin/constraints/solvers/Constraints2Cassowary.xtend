package fr.eseo.atol.gen.plugin.constraints.solvers

import com.google.common.collect.BiMap
import com.google.common.collect.HashBiMap
import fr.eseo.atlc.constraints.ArExp
import fr.eseo.atlc.constraints.BinaryExp
import fr.eseo.atlc.constraints.ConstantExp
import fr.eseo.atlc.constraints.DoubleExp
import fr.eseo.atlc.constraints.IntExp
import fr.eseo.atlc.constraints.NaryExp
import fr.eseo.atlc.constraints.SimpleConstraint
import fr.eseo.atlc.constraints.StrengthLevel
import fr.eseo.atlc.constraints.UnaryExp
import fr.eseo.atlc.constraints.VariableExp
import fr.eseo.atlc.constraints.VariableVectorExp
import fr.eseo.atol.gen.plugin.constraints.common.Boxable
import fr.eseo.atol.gen.plugin.constraints.common.Constraints
import fr.eseo.atol.gen.plugin.constraints.common.ConstraintsHelpers
import fr.eseo.atol.gen.plugin.constraints.common.EMFBoxable
import fr.eseo.atol.gen.plugin.constraints.common.JFXBoxable
import java.util.ArrayList
import java.util.Collection
import java.util.HashMap
import java.util.List
import java.util.Map
import javafx.beans.property.DoubleProperty
import javafx.scene.shape.Rectangle
import javafx.scene.text.Text
import org.eclipse.papyrus.aof.core.AOFFactory
import org.eclipse.papyrus.aof.core.IBox
import org.eclipse.papyrus.aof.core.impl.utils.DefaultObserver
import org.pybee.cassowary.AbstractConstraint
import org.pybee.cassowary.Constraint
import org.pybee.cassowary.Expression
import org.pybee.cassowary.SimplexSolver
import org.pybee.cassowary.StayConstraint
import org.pybee.cassowary.Strength
import org.pybee.cassowary.Variable
import org.pybee.cassowary.VariableObserver

class Constraints2Cassowary {
	extension Constraints cstrExt = new Constraints
	extension ConstraintsHelpers cstrHelp
	extension JFXBoxable jfxBoxable
	extension EMFBoxable curBoxable

	var Collection<? extends Boxable> boxables

	val SimplexSolver solver = new SimplexSolver

	val BiMap<Boxable.Property, Variable> cacheVar = HashBiMap.create
	val Map<Object, Variable> prop2Var = new HashMap

	var IBox<fr.eseo.atlc.constraints.Constraint> constraints_in
	var IBox<fr.eseo.atlc.constraints.Constraint> constraints_flat
	var IBox<AbstractConstraint> constraints_cass

	val List<Pair<Variable, Double>> suggestList = new ArrayList

	var failed = false

	val varObs = new VariableObserver {
		override onVariableChanged(Variable it) {
			if (attachedObject !== null) {
				val prop = attachedObject as DoubleProperty
//				println('''Variable «name» changed value from «prop.value» to «value»''')
				prop.set(value)
			}
		}
	}

	def sync() {
		cacheVar.forEach[prop, it |
			(attachedObject as DoubleProperty).set(value)
		]
	}

	var solving = false

	val (Object,String)=>fr.eseo.atlc.constraints.Expression fallback
	new(Object jfxMM, Object curMM) {
		this(jfxMM, curMM)[]
	}

	new(Object jfxMM, Object curMM, (Object,String)=>fr.eseo.atlc.constraints.Expression fallback) {
		jfxBoxable = new JFXBoxable(jfxMM)
		curBoxable = new EMFBoxable(curMM)
		this.fallback = fallback
		boxables = #[jfxBoxable, curBoxable].toList

		cstrHelp = new ConstraintsHelpers(boxables, cstrExt)
		solver.autosolve = true
	}

	def void solve() {
		if (!solving) {
			solving = true
			solver.solve
			solving = false
		}
	}

	def void resolve() {
		solver.resolve
	}

	def getFailed() {
		failed
	}

	def setFailed(boolean failed) {
		this.failed = failed
	}

	def void debug() {
		println('''
			*****************************
			Constraints raw :
			«FOR c : constraints_in»
				- «c.prettyPrint»
			«ENDFOR»
			Constraints flat :
			«FOR c : constraints_flat»
				- «c.prettyPrint»
			«ENDFOR»
			Constraints casso :
			«FOR c : constraints_cass»
				- «c»
			«ENDFOR»
			*****************************
		''')
	}

	def hasVariable(DoubleProperty p) {
		prop2Var.containsKey(p)
	}

	def suggestValue(DoubleProperty p, double value) {
		if (prop2Var.containsKey(p)) {
			suggest(prop2Var.get(p), value, Strength.STRONG)
		}
		else {
			throw new UnsupportedOperationException('''No variable found for «p» in cache''')
		}
	}

	def suggestValue(DoubleProperty p, double value, StrengthLevel strength) {
		if (prop2Var.containsKey(p)) {
			suggest(prop2Var.get(p), value, strength.toCassowaryStrength)
		}
		else {
			throw new UnsupportedOperationException('''No variable found for «p» in cache''')
		}
	}

	public val EPSILON = 1E-2
	def void suggest(Variable v, double value, Strength s) {
		// TODO : correctly handle strength here
		// TODO : what if we have multiple updates ?
		if (Math.abs(v.value - value) > EPSILON) {
			solver.addEditVar(v, s)
			solver.beginEdit
			solver.suggestValue(v, value - v.value)
			solver.endEdit
		}
	}

	val emptyCstr = AOFFactory.INSTANCE.<fr.eseo.atlc.constraints.Constraint>createOrderedSet
	def apply(IBox<fr.eseo.atlc.constraints.Constraint> constraints) {
		// add listener on Constraint to get notified when update in children
		constraints_in = constraints
		constraints_flat = constraints.flattenConstraints.collectMutable[
			it?.simplifyConstraint ?: emptyCstr
		] //.inspect("cstr simplified : ")
		constraints_cass = constraints_flat.collect[convert] //.inspect("cstr casso : ")

		// Setup observer on res
		constraints_cass.addObserver(new DefaultObserver<AbstractConstraint>{
			override added(int index, AbstractConstraint element) {
				failed = !solver.addConstraintNoException(element) || failed
				applySuggest
			}

			override moved(int newIndex, int oldIndex, AbstractConstraint element) {
			}

			override removed(int index, AbstractConstraint element) {
				solver.removeConstraint(element) //TODO : check for unused variables
				//TODO: otherwise memory leak
			}

			override replaced(int index, AbstractConstraint newElement, AbstractConstraint oldElement) {
				removed(index, oldElement)
				added(index, newElement)
			}
		})
		// Manually add variables already present in res
		constraints_cass.forEach[
			if (it !== null) {
				solver.addConstraint(it)
			}
		]
		applySuggest
	}

	def applySuggest() {
		suggestList.forEach[
			suggest(key, value, Strength.STRONG)
		]
		suggestList.clear
	}

	dispatch def AbstractConstraint convert(Constraint it) {
		throw new UnsupportedOperationException('''Cannot convert «it»''')
	}

	dispatch def AbstractConstraint convert(SimpleConstraint it) {
		if (arguments.length > 2) {
			throw new UnsupportedOperationException("Wrong number of arguments")
		}
		val s = strength.strength.toCassowaryStrength
		val w = strength.weight

		if (predicate == "stay") {
			val v = arguments.get(0).convertVar
			new StayConstraint(v, s, w)
		} else if (predicate == "suggest") {
			val variable = arguments.get(0).convertVar
			val value = arguments.get(1) as DoubleExp
			suggestList.add((variable -> value.value))
			null //TODO: can this be done in a better way ?
		} else {
			val l = arguments.get(0).convertExp
			val r = arguments.get(1).convertExp
			var op = switch predicate {
				case "<=",
				case ".<=": Constraint.Operator.LEQ
				case ">=",
				case ".>=": Constraint.Operator.GEQ
				case "=",
				case ".=": Constraint.Operator.EQ

				default: throw new UnsupportedOperationException('''Unknown constraint «predicate»''')
			}
			new Constraint(l, op, r, s, w)
		}
	}

//@begin convertExp
	def dispatch Expression convertExp(fr.eseo.atlc.constraints.Expression it) {
		throw new UnsupportedOperationException('''Cannot convert «it» to expression''')
	}

	def dispatch Expression convertExp(NaryExp it) {
		val ops = operands.map[convertExp]
		switch operator {
			case 'sum':
				ops.fold(new Expression(0), [$0.plus($1)])
			default:
				throw new UnsupportedOperationException('''Unknown operator «operator»''')
		}
	}

	def dispatch Expression convertExp(BinaryExp it) {
		val l = left.convertExp
		val r = right.convertExp
		switch operator {
			case ".+",
			case "+":
				l.plus(r)
			case ".-",
			case "-":
				l.minus(r)
			case ".*",
			case "*":
				l.times(r)
			case "./",
			case "/":
				l.divide(r)
			default:
				throw new UnsupportedOperationException('''Unknown operator «operator»''')
		}
	}

	def dispatch Expression convertExp(UnaryExp it) {
		val arg = operand.convertExp
		switch operator {
			case '-',
			case 'neg':
				arg.times(-1)
			default:
				throw new UnsupportedOperationException('''Unknown operator «operator»''')
		}
	}

	def dispatch Expression convertExp(DoubleExp it) {
		new Expression(value)
	}

	def dispatch Expression convertExp(IntExp it) {
		new Expression(value)
	}

	def dispatch Expression convertExp(VariableExp it) {
		var Expression ret = null
		var v = convertVar
		if(v !== null) {
			ret = new Expression(v)
		}
		if(ret === null) {
			ret = fallback.apply(source, propertyName)?.convertExp
		}
		if (ret === null) {
			throw new UnsupportedOperationException('''No suitable method to get property «propertyName» of «source»''')
		}
		ret
	}

	def dispatch Expression convertExp(VariableVectorExp it) {
		throw new UnsupportedOperationException('''Expressions containing VariableVector must be flattened first''')
	}
//@end convertExp

//@begin convertVar
	def dispatch Variable convertVar(Expression it) {
		throw new UnsupportedOperationException('''Cannot convert «it» to variable''')
	}

	def dispatch Variable convertVar(VariableExp it) {
		source.createVariable(propertyName)
	}
//@end convertVar
	//TODO: this is ugly, ask Fred for help
	def createVariable(Object o, String propertyName) {
		var Variable variable = null
		for (b : boxables) {
			if (variable === null && b.hasProperty(o, propertyName)) {
				val prop = b.getProperty(o, propertyName)
				if (!cacheVar.containsKey(prop)) {
					variable = new Variable => [
						attachedObject = prop.property
						name = b.getName(attachedObject)
						observer = varObs
					]
					prop2Var.put(prop.property, variable)
					cacheVar.put(prop, variable)
				}
				else {
					variable = cacheVar.get(prop)
				}
			}
		}
		variable
	}

	static def toCassowaryStrength(StrengthLevel it) {
		switch it {
			case REQUIRED: Strength.REQUIRED
			case STRONG: Strength.STRONG
			case MEDIUM: Strength.MEDIUM
			case WEAK: Strength.WEAK
		}
	}

	// TODO: move to ConstraintsHelpers
	static def binary(String op, ArExp l, ArExp r) {
		Constraints.BinaryExp.newInstance=>[
			left = l
			operator = op
			right = r
		]
	}
	static def dbl(double v) {
		Constraints.DoubleExp.newInstance=>[
			value = v
		]
	}
	static def vare(Object o, String pn) {
		Constraints.VariableExp.newInstance=>[
			source = o
			propertyName = pn
		]
	}
	static def conste(Object o, String pn) {
		Constraints.ConstantExp.newInstance=>[
			source = o
			propertyName = pn
		]
	}
	public static val geometricAbstraction = [Object o, String propName |
		switch o {
			Text: switch propName {
				case "centerX":	binary("+",
									o.vare("x"),
									binary("/",
										o.conste("width"),
										dbl(2)
									)
								)
				default: null
			}
			Rectangle: switch propName {
				case "centerX":	binary("+",
									o.vare("x"),
									binary("/",
										o.vare("width"),
										dbl(2)
									)
								)
				default: null
			}
			default: null
		}
	]
	// for abstraction layers, otherwise already flattened during the previous step
//	def dispatch Expression convertExp(ConstantExp it) {
//		// TODO: make incremental => toConstant should be flattened during the previous step
//		new Expression(createVariable(source, propertyName).value)
//	}
}
