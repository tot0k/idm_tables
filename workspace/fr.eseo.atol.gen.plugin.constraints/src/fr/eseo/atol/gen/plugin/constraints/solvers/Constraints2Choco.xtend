package fr.eseo.atol.gen.plugin.constraints.solvers

import com.google.common.collect.BiMap
import com.google.common.collect.HashBiMap
import fr.eseo.atlc.constraints.ArExp
import fr.eseo.atlc.constraints.BinaryExp
import fr.eseo.atlc.constraints.BinaryRelExp
import fr.eseo.atlc.constraints.Constraint
import fr.eseo.atlc.constraints.DoubleExp
import fr.eseo.atlc.constraints.Expression
import fr.eseo.atlc.constraints.IntExp
import fr.eseo.atlc.constraints.NaryExp
import fr.eseo.atlc.constraints.ReifyExp
import fr.eseo.atlc.constraints.SimpleConstraint
import fr.eseo.atlc.constraints.Strength
import fr.eseo.atlc.constraints.StrengthLevel
import fr.eseo.atlc.constraints.UnaryExp
import fr.eseo.atlc.constraints.UnaryRelExp
import fr.eseo.atlc.constraints.VariableExp
import fr.eseo.atlc.constraints.VariableRelationExp
import fr.eseo.atlc.constraints.VariableRelationVectorExp
import fr.eseo.atlc.constraints.VariableVectorExp
import fr.eseo.atol.gen.plugin.constraints.common.Boxable
import fr.eseo.atol.gen.plugin.constraints.common.Constraints
import fr.eseo.atol.gen.plugin.constraints.common.ConstraintsHelpers
import fr.eseo.atol.gen.plugin.constraints.common.EMFBoxable
import fr.eseo.atol.gen.plugin.constraints.common.JFXBoxable
import fr.eseo.atol.gen.plugin.constraints.common.RelationBoxable
import java.util.ArrayList
import java.util.Collection
import java.util.HashMap
import java.util.Map
import org.chocosolver.solver.Model
import org.chocosolver.solver.Solution
import org.chocosolver.solver.expression.discrete.arithmetic.ArExpression
import org.chocosolver.solver.expression.discrete.relational.ReExpression
import org.chocosolver.solver.variables.IntVar
import org.eclipse.emf.ecore.EObject
import org.eclipse.papyrus.aof.core.AOFFactory
import org.eclipse.papyrus.aof.core.IBox

class Constraints2Choco {
	extension Constraints cstrExt = new Constraints
	extension ConstraintsHelpers cstrHelp

	var Collection<? extends Boxable> boxables
	val RelationBoxable relBox = new RelationBoxable

	val BiMap<Boxable.Property, ArExpression> cacheVar = HashBiMap.create
	val Map<Object, ArExpression> prop2Var = new HashMap
	val Map<ArExp, IntVar> intermediateVariables = new HashMap
	val Map<ArExpression, ArExpression> relationVarCache = new HashMap

	private static class Suggestion {
		val Strength strength
		var int value
		val boolean stay

		new(Strength strength, int value, boolean stay) {
			this.strength = strength
			this.value = value
			this.stay = stay
		}
	}

	val Map<Object, Suggestion> suggestedValues = new HashMap

	var Model model
	var Solution solution
	var Solution lastSolution

	var IBox<Constraint> constraintsIn
	var IBox<Constraint> constraintsFlat

	var DOM_LB = -5000
	var DOM_UB = 5000

	new(Object jfxMM, Object curMM) {
		boxables = #[
			new JFXBoxable(jfxMM),
			new EMFBoxable(curMM),
			relBox
		]
		cstrHelp = new ConstraintsHelpers(boxables, cstrExt)
	}

	def getDefaultUpperBound() {
		DOM_UB
	}

	def getDefaultLowerBound() {
		DOM_LB
	}

	def setDefaultUpperBound(int ub) {
		DOM_UB = ub
	}

	def setDefaultLowerBound(int lb) {
		DOM_LB = lb
	}

	def boolean solve() {
		var solved = false

		postConstraints
		if (!suggestedValues.empty) {
			// we have an optimization problem, stop when search is finished
			while (model.solver.solve) {
				solution.record
				solved = true
			}
		}
		else {
			// satisfaction problem, we only take the first solution
			if (model.solver.solve) {
				solution.record
				solved = true
			}
		}
		if (solved) {
			debug
			updateProperties
			lastSolution = solution
		}
		solved
	}

	def void updateProperties() {
		cacheVar.forEach[prop, variable |
			val v = variable as IntVar
			val value = solution.getIntVal(v)
			// update current values for stayed variables
			suggestedValues.filter[$1.stay].forEach[p1, p2|
				p2.value = value
			]
			// remove old suggestions as they only apply for the next solve
			val toRemove = suggestedValues.filter[!$1.stay].keySet.clone
			toRemove.forEach[suggestedValues.remove(it)]

			val boxable = prop.boxable

			if (boxable.getPropertyValue(prop) != value) {
				boxable.setPropertyValue(prop, value)
			}
		]
	}

	def void suggest(IBox<Integer> property, int value) {
		if (prop2Var.containsKey(property)) {
			val s = Constraints.Strength.newInstance => [
				strength = StrengthLevel.STRONG //TODO: add parameter to control strength ?
				weight = 1
			]
			suggestedValues.put(property, new Suggestion(s, value, false))
		}
		else {
			throw new UnsupportedOperationException('''Cannot find variable corresponding to property «property»''')
		}
	}

	def void suggest(EObject it, String relation, EObject value) {
		if (prop2Var.containsKey(it)) {
			val s = Constraints.Strength.newInstance => [
				strength = StrengthLevel.STRONG //TODO: add parameter to control strength ?
				weight = 1
			]
			val valueId = relBox.getIdOf(value)
			suggestedValues.put(it, new Suggestion(s, valueId, false))
		}
		else {
			throw new UnsupportedOperationException('''Cannot find variable corresponding to property «it»''')
		}
	}

	def void debug() {
		println('''
			*****************************
			Constraints raw :
			«FOR c : constraintsIn»
				- «c.prettyPrint»
			«ENDFOR»
			Constraints flat :
			«FOR c : constraintsFlat»
				- «c.prettyPrint»
			«ENDFOR»
			Choco solver:
			«model»
			Last solution:
			«lastSolution»
		''')
		println('''
			Properties values:
		''')
		for(v : cacheVar.entrySet.map[
					key.boxable.getName(key.property) -> solution.getIntVal(value as IntVar)
				]) {
			println('''«v.key» -> «v.value»''')
		}
		println('''
			Relations Ids:
				«relBox.debug»
			*****************************
		''')
	}

	val emptyCstr = AOFFactory.INSTANCE.<Constraint>createOrderedSet
	def apply(IBox<Constraint> constraints) {
		constraintsIn = constraints//.inspect("Cstr in : ")
		constraintsFlat = constraints.flattenConstraints.collectMutable[
			it?.simplifyConstraint ?: emptyCstr
		]//.inspect("cstr simplified : ")
	}

	def postConstraints() {
		cacheVar.clear
		prop2Var.clear
		intermediateVariables.clear
		relationVarCache.clear
		model = new Model
		solution = new Solution(model)

		val cstrs = constraintsFlat.map[cstr2Choco]
		cstrs.forEach[it?.post]

		if (suggestedValues.size > 0) {
			val errors = new ArrayList<Pair<IntVar, Integer>>
			suggestedValues.forEach[prop, suggestion|
				var IntVar v
				if (prop2Var.containsKey(prop)) {
					v = prop2Var.get(prop) as IntVar
				}
				else if (intermediateVariables.containsKey(prop)) {
					v = intermediateVariables.get(prop)
				}
				else {
					throw new UnsupportedOperationException("Cannot find variable in caches")
				}

				if (suggestion.strength.strength == StrengthLevel.REQUIRED) {
					v.dist(suggestion.value).eq(0).post
				}
				else {
					val err = model.intVar("error_"+v.name, -100, 100)
					v.dist(suggestion.value).eq(err).post
					errors.add(err -> suggestion.strength.convertStrength)
				}
			]
			val errSum = model.intVar("error_sum", 0, 1000)
			model.scalar(errors.map[key], errors.map[value], '=', errSum).post
			model.setObjective(Model.MINIMIZE, errSum)
		}
	}
/*************************************/
	def cstr2Choco(Constraint it) {
		val cstr = it as SimpleConstraint
		switch cstr.predicate {
			case 'allDifferent':
				model.allDifferent(cstr.arguments.map[convertExp.intVar])
			case 'stay': {
				val arg = cstr.arguments.map[convertExp]
				if (arg.size > 1) {
					throw new UnsupportedOperationException("Cannot apply stay to a collection")
				}

				// TODO: check if this works for expressions
				if (lastSolution !== null) {
					val v = arg.get(0).intVar
					try {
						val value = lastSolution.getIntVal(v)
						suggestedValues.put(cstr.arguments.get(0), new Suggestion(cstr.strength, value, true))
						intermediateVariables.put(cstr.arguments.get(0), v)
					}
					catch(Exception e) {
						println("Variable has not been found in previous solution, skipping it for now.")
					}
				}
				null
			}
			default: {
				convert.decompose
			}
		}
	}

	dispatch def ReExpression convert(Constraint it) {
		throw new UnsupportedOperationException('''Cannot convert «it»''')
	}

	dispatch def ReExpression convert(BinaryRelExp it) {
		val l = left.convert
		val r = right.convert
		val cstr = switch operator {
			case "&&",
			case "and": l.and(r)
			case "||",
			case "or": l.or(r)
			case "xor": l.xor(r)
			case "->",
			case "imp": l.imp(r)
			default: throw new UnsupportedOperationException('''Unsupported predicate «operator»''')
		}
		cstr
	}

	dispatch def ReExpression convert(UnaryRelExp it) {
		val arg = operand.convert
		val cstr = switch operator {
			case "!",
			case "not": arg.not
			default: throw new UnsupportedOperationException('''Unsupported predicate «operator»''')
		}
		cstr
	}

	dispatch def ReExpression convert(SimpleConstraint it) {
		if (arguments.length > 2) {
			throw new UnsupportedOperationException("Predicate with arity > 2 are not supported yet.")
		}
		val l = arguments.get(0).convertExp
		val r = arguments.get(1).convertExp
		val cstr = switch predicate {
			case "<=",
			case ".<=": l.le(r)
			case "<",
			case ".<": l.lt(r)
			case ">=",
			case ".>=": l.ge(r)
			case ">",
			case ".>": l.gt(r)
			case "=",
			case ".=": l.eq(r)
			case "!=",
			case ".!=",
			case "<>",
			case ".<>": l.ne(r)
			default: throw new UnsupportedOperationException('''Unsupported predicate «predicate»''')
		}
		cstr
	}

//@begin convertExp
	dispatch def ArExpression convertExp(Expression it) {
		throw new UnsupportedOperationException('''Cannot process «it»''')
	}

	dispatch def ArExpression convertExp(NaryExp it) {
		val ops = operands.map[convertExp]
		switch operator {
			case 'sum': {
				ops.get(0).add(ops.drop(1))
			}
			case 'product': {
				ops.get(0).mul(ops.drop(1))
			}
			default:
				throw new UnsupportedOperationException('''Unknown operator «operator»''')
		}
	}

	dispatch def ArExpression convertExp(BinaryExp it) {
		val l = left.convertExp
		val r = right.convertExp
		switch operator {
			case ".+",
			case "+":
				l.add(r)
			case ".-",
			case "-":
				l.sub(r)
			case ".*",
			case "*":
				l.mul(r)
			case "./",
			case "/":
				l.div(r)
			case '^',
			case 'pow':
				l.pow(r)
			case 'min':
				l.min(r) //TODO: min, max support more than 2 arguments, it could be used as unary operation on VariableVector ?
			case 'max':
				l.max(r)
			case 'dist':
				l.dist(r)
			case 'mod':
				l.mod(r)
			default:
				throw new UnsupportedOperationException('''Unknown operator «operator»''')
		}
	}

	dispatch def ArExpression convertExp(ReifyExp it) {
		val arg = operand.convert
		arg.intVar
	}

	dispatch def ArExpression convertExp(UnaryExp it) {
		val arg = operand.convertExp

		switch operator {
			case '-',
			case 'neg':
				arg.neg
			case 'abs':
				arg.abs
			case 'sqr':
				arg.sqr
			default:
				throw new UnsupportedOperationException('''Unknown operator «operator»''')
		}
	}

	var warnedDoubleCast = false
	dispatch def ArExpression convertExp(DoubleExp it) {
		if (!warnedDoubleCast) {
			println('''Choco does not support Double values, casting to int («it.prettyPrint»)''')
			warnedDoubleCast = true
		}
		model.intVar(value.intValue)
	}

	dispatch def ArExpression convertExp(IntExp it) {
		model.intVar(value)
	}

	dispatch def ArExpression convertExp(VariableExp it) {
		source.createVariable(propertyName)
	}

	dispatch def ArExpression convertExp(VariableRelationExp it) {
		val variable = source.createVariable('''«relationName»''')
		if (relationVarCache.containsKey(variable)) {
			return relationVarCache.get(variable)
		}
		else {
			val candidates = relBox.getCandidates(source as EObject, relationName, propertyPath)
			val prop = relBox.getProperty(source, relationName)

			val relVar = model.intVar('''foo_«relBox.getName(source)».«propertyPath»''', prop.lowerBound.intValue, prop.upperBound.intValue)
			candidates.forEach[println(it)]
//			model.sum(belongVars, '=', 1).post //TODO: it seams useless, so let ignore it for now
			model.scalar(candidates.map[
							variable.eq(key).intVar
						],
						candidates.map[
							value
						],
						'=',
						relVar).post

			relationVarCache.put(variable, relVar)
			relVar
		}
	}

	def dispatch ArExpression convertExp(VariableVectorExp it) {
		throw new UnsupportedOperationException('''Expressions containing VariableVector must be flattened first''')
	}

	def dispatch ArExpression convertExp(VariableRelationVectorExp it) {
		throw new UnsupportedOperationException('''Expressions containing VariableRelationVector must be flattened first''')
	}
//@end convertExp
	def createVariable(Object o, String propertyName) {
		var ArExpression variable = null
		for (b : boxables) {
			if (variable === null && b.hasProperty(o, propertyName)) {
				val prop = b.getProperty(o, propertyName)
				if (!cacheVar.containsKey(prop)) {
					if (prop.isUnbounded) {
						variable = model.intVar(b.getName(prop.property), DOM_LB, DOM_UB)
					}
					else {
						variable = model.intVar(b.getName(prop.property), prop.lowerBound.intValue, prop.upperBound.intValue)
					}
					cacheVar.put(prop, variable)
					prop2Var.put(prop.property, variable)
				}
				else {
					variable = cacheVar.get(prop)
				}
			}
		}
		if (variable === null) {
			throw new NoSuchMethodException('''No suitable method to get property «propertyName» of «o»''')
		}
		variable
	}

	def int convertStrength(Strength it) {
		return (weight as int) * switch strength {
			case REQUIRED: -1
			case STRONG: 10000
			case MEDIUM: 100
			case WEAK: 1
		}
	}
}
