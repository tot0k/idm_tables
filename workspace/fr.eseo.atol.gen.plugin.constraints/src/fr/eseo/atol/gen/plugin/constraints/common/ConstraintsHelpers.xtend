package fr.eseo.atol.gen.plugin.constraints.common

import fr.eseo.atlc.constraints.ArExp
import fr.eseo.atlc.constraints.BinaryExp
import fr.eseo.atlc.constraints.BinaryRelExp
import fr.eseo.atlc.constraints.ConstantExp
import fr.eseo.atlc.constraints.ConstantVectorExp
import fr.eseo.atlc.constraints.Constraint
import fr.eseo.atlc.constraints.ConstraintGroup
import fr.eseo.atlc.constraints.DoubleExp
import fr.eseo.atlc.constraints.Expression
import fr.eseo.atlc.constraints.IntExp
import fr.eseo.atlc.constraints.ReifyExp
import fr.eseo.atlc.constraints.RelExp
import fr.eseo.atlc.constraints.SimpleConstraint
import fr.eseo.atlc.constraints.UnaryExp
import fr.eseo.atlc.constraints.UnaryRelExp
import fr.eseo.atlc.constraints.VariableExp
import fr.eseo.atlc.constraints.VariableRelationExp
import fr.eseo.atlc.constraints.VariableRelationVectorExp
import fr.eseo.atlc.constraints.VariableVectorExp
import java.util.Collection
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.papyrus.aof.core.AOFFactory
import org.eclipse.papyrus.aof.core.IBox
import org.eclipse.papyrus.aof.core.impl.utils.DefaultObserver

import static extension fr.eseo.aof.exploration.OCLByEquivalence.*

class ConstraintsHelpers {
	extension Constraints cstrExt
	extension Collection<? extends Boxable> boxables
	val AOF = AOFFactory.INSTANCE
	// TODO: is it safe to have multiple instances of this extension ?

	new(Collection<? extends Boxable> boxables, Constraints cstrExt) {
		this.boxables = boxables
		this.cstrExt = cstrExt
	}

	def flattenConstraints(IBox<Constraint> constraints) {
		val sc = constraints.select(SimpleConstraint).collect[it as Constraint]
		val re = constraints.select(RelExp).collect[it as Constraint]
		val cg = cstrExt.constraints(constraints.select(ConstraintGroup).closure[
			 it._constraints.select(ConstraintGroup)
		])
		sc.concat(
			re
		).concat(
			cg.select(SimpleConstraint).collect[it as Constraint]
		).concat(
			cg.select(RelExp).collect[it as Constraint]
		)
	}

//@begin simplifyConstraint
	dispatch def IBox<Constraint> simplifyConstraint(Constraint c) {
		throw new UnsupportedOperationException('''Cannot simplify «c»''')
	}

	dispatch def IBox<Constraint> simplifyConstraint(UnaryRelExp c) {
		val arg = c.operand.simplifyConstraint
		arg.collect[a |
			val res = Constraints.UnaryRelExp.newInstance => [
				operator = c.operator
				operand = a
			]
			res as Constraint
		]
	}

	dispatch def IBox<Constraint> simplifyConstraint(BinaryRelExp c) {
		val left = c.left.simplifyConstraint
		val right = c.right.simplifyConstraint

		if (c.operator.isScalar) {
			left.zipWith(right, [l, r |
				val ret = Constraints.BinaryRelExp.newInstance => [
					left = l
					right = r
					operator = c.operator
				]
				return ret
			])
		}
		else {
			left.collectMutable[l |
				if (l === null) {
					emptyCstr
				}
				else {
					right.collect[r |
						val b = Constraints.BinaryRelExp.newInstance => [
							left = EcoreUtil.copy(l) //TODO : is copy necessary here ?
							right = EcoreUtil.copy(r)
							operator = c.operator
						]
						return b as Constraint
					]
				}
			]
		}
	}

	val emptyCstr = AOF.<Constraint>createOrderedSet
	dispatch def IBox<Constraint> simplifyConstraint(SimpleConstraint c) {
		if (c.arguments.size > 2) {
			throw new UnsupportedOperationException('''Cannot expand constraints with more than 2 arguments «c»''')
		}
		val expressions = c.arguments.map[simplifyAr]
		if (c.arguments.size == 0) {
			throw new RuntimeException('''No arguments''')
		}
		else if (c.arguments.size == 1) {
			if (c.predicate.isArrayPredicate) {
				val res = Constraints.SimpleConstraint.newInstance => [
					predicate = c.predicate
					strength = EcoreUtil.copy(c.strength)
					arguments.addAll(expressions.get(0)) //TODO: this is not active, should it be done in a active way ? (do not forget to create a new Constraint when recreating)
				]
				AOF.createOne(res as Constraint)
			}
			else {
				expressions.get(0).collect[a |
					val res = Constraints.SimpleConstraint.newInstance => [
						predicate = c.predicate
						strength = EcoreUtil.copy(c.strength) //TODO : strength is not copied in a active way
						arguments.add(a)
					]
					res as Constraint
				]
			}
		}
		else {
			val a1 = expressions.get(0)
			val a2 = expressions.get(1)
			if (c.predicate.isScalar) {
				a1.zipWith(a2, [l, r |
					val res = Constraints.SimpleConstraint.newInstance => [
						predicate = c.predicate
						strength = EcoreUtil.copy(c.strength)
						arguments.addAll(l, r)
					]
					res as Constraint
				])
			}
			else {
				a1.collectMutable[l |
					if (l === null) {
						emptyCstr
					}
					else {
						val res = a2.collect[r |
							val res = Constraints.SimpleConstraint.newInstance => [
								predicate = c.predicate
								strength = EcoreUtil.copy(c.strength)
								arguments.addAll(EcoreUtil.copy(l), EcoreUtil.copy(r))
							]
							res as Constraint
						]
						res as IBox<Constraint>
					}
				]
			}
		}
	}
//@end simplifyConstraint

//@begin simplify
	def dispatch IBox<ArExp> simplifyAr(Expression it) {
		throw new UnsupportedOperationException('''Cannot simplify «it.prettyPrint»''')
	}

	def dispatch IBox<ArExp> simplifyAr(VariableExp it) {
		val v = Constraints.VariableExp.newInstance
		v.source = source
		v.propertyName = propertyName
		AOF.createOne(v)
	}

	def dispatch IBox<ArExp> simplifyAr(VariableRelationExp it) {
		val v = Constraints.VariableRelationExp.newInstance
		v.source = source
		v.propertyPath = propertyPath
		v.relationName = relationName
		AOF.createOne(v)
	}

	def dispatch IBox<ArExp> simplifyAr(ConstantExp it) {
		source.toReadOnlyBox(propertyName).collect[v |
			val r = Constraints.DoubleExp.newInstance => [
				value = v?.doubleValue
			]
			r
		]
	}

	def dispatch IBox<ArExp> simplifyAr(DoubleExp it) {
		val d = Constraints.DoubleExp.newInstance
		d.value = value
		AOF.createOne(d)
	}

	def dispatch IBox<ArExp> simplifyAr(IntExp it) {
		val d = Constraints.IntExp.newInstance
		d.value = value
		AOF.createOne(d)
	}

	def dispatch IBox<ArExp> simplifyAr(VariableVectorExp v) {
		val variables = v.source as IBox<Object>
		variables.select[
			it !== null
		].collect[s |
			val e = Constraints.VariableExp.newInstance => [
				source = s
				propertyName = v.propertyName
			]
			e
		]
	}

	def dispatch IBox<ArExp> simplifyAr(VariableRelationVectorExp v) {
		val variables = v.source as IBox<Object>
		variables.select[
			it !== null
		].collect[s |
			val e = Constraints.VariableRelationExp.newInstance => [
				source = s
				relationName = v.relationName
				propertyPath = v.propertyPath
			]
			e
		]
	}

	val emptyCst = AOF.<ArExp>createOrderedSet
	def dispatch IBox<ArExp> simplifyAr(ConstantVectorExp it) {
		val props = source as IBox<Object>
		props.collectMutable[s |
			if (s === null) {
				emptyCst
			}
			else {
				s.toReadOnlyBox(propertyName).collect[v |
					val r = Constraints.DoubleExp.newInstance => [
						value = v?.doubleValue
					]
					r as ArExp
				]
			}
		]
	}

	public static var RIGHT_LEFT_DEP = false
	val emptyExp = AOF.<ArExp>createOrderedSet
	def dispatch IBox<ArExp> simplifyAr(BinaryExp e) {
		val left = e.left.simplifyAr
		val right = e.right.simplifyAr
		if (e.operator.isScalar) {
			// FIXME: this works for Curriculum transfo but won't in the general case
			// right now I don't have any better idea ...
			//TODO: do dynamic analysis, if there is an intersection between properties used in operands then we have a left->right dependency
			left.zipWith(right, RIGHT_LEFT_DEP, [l, r |
				val ret = Constraints.BinaryExp.newInstance => [
					left = l
					right = r
					operator = e.operator
				]
				return ret
			], [])
		}
		else {
			left.collectMutable[l |
				if (l === null) {
					emptyExp
				}
				else {
					right.collect[r |
						val b = Constraints.BinaryExp.newInstance => [
							left = EcoreUtil.copy(l) //TODO : is copy necessary here ?
							right = EcoreUtil.copy(r)
							operator = e.operator
						]
						return b as ArExp
					]
				}
			]
		}
	}

	dispatch def IBox<ArExp> simplifyAr(UnaryExp e) { //TODO: fix me
		val arg = e.operand.simplifyAr
		if (e.operator.isArrayPredicate) {
			val res = AOF.<ArExp>createOption()
			res.set(Constraints.NaryExp.newInstance => [
				operator = e.operator
				operands.addAll(arg)
			])

			arg.addObserver(new DefaultObserver<ArExp>() {
					override added(int index, ArExp element) {
						recreate
					}

					override moved(int newIndex, int oldIndex, ArExp element) {
						recreate
					}

					override removed(int index, ArExp element) {
						recreate
					}

					override replaced(int index, ArExp newElement, ArExp oldElement) {
						recreate
					}

					def recreate() {
						res.set(Constraints.NaryExp.newInstance => [
							operator = e.operator
							operands.addAll(arg)
						])
					}
				})
			return res
		}
		else {
			arg.collect[a |
				val r = Constraints.UnaryExp.newInstance => [
					operator = e.operator
					operand = a
				]
				r
			]
		}
	}

	dispatch def IBox<ArExp> simplifyAr(ReifyExp it) {
		val ops = operand.simplifyConstraint
		ops.collect[o |
			var r = Constraints.ReifyExp.newInstance => [
				operand = o
			]
			r
		]
	}
//@end simplify

	def toReadOnlyBox(Object o, String propertyName) {
		var IBox<Number> r = null
		for (b : boxables) {
			if (r === null &&
				b.hasProperty(o, propertyName)
			) {
				r = b.toReadOnlyBox(o, propertyName)
			}
		}
		if (r === null) {
			throw new NoSuchMethodException('''Cannot convert property «propertyName» of «o.class.simpleName» to constant''')
		}
		else {
			r
		}
	}

	def isScalar(String op) { //TODO: +,-,*,/ and =,>,<,>=,>= have opposite meaning with scalar/cartesian
		switch (op) {
			case '+',
			case '-',
			case '*',
			case '/',
			case '^':
				true

			case '.+',
			case '.-',
			case '.*',
			case './',
			case '.^':
				false

			case '>',
			case '<',
			case '>=',
			case '<=',
			case '=',
			case '!=',
			case '<>':
				true

			case '.>',
			case '.<',
			case '.<=',
			case '.>=',
			case '.=',
			case '.!=',
			case '.<>':
				false

			default: true
		}
	}

	def isArrayPredicate(String pred) {
		switch pred {
			case 'sum',
			case 'product',
			case 'allDifferent': true

			default: false
		}
	}
}
