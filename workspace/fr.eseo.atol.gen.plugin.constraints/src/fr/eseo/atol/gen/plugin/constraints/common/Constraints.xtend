package fr.eseo.atol.gen.plugin.constraints.common

import fr.eseo.aof.xtend.utils.AOFAccessors
import fr.eseo.atlc.constraints.ArExp
import fr.eseo.atlc.constraints.BinaryExp
import fr.eseo.atlc.constraints.BinaryRelExp
import fr.eseo.atlc.constraints.ConstantExp
import fr.eseo.atlc.constraints.ConstantVectorExp
import fr.eseo.atlc.constraints.Constraint
import fr.eseo.atlc.constraints.ConstraintGroup
import fr.eseo.atlc.constraints.ConstraintsPackage
import fr.eseo.atlc.constraints.DoubleExp
import fr.eseo.atlc.constraints.Expression
import fr.eseo.atlc.constraints.IntExp
import fr.eseo.atlc.constraints.NaryExp
import fr.eseo.atlc.constraints.NaryRelExp
import fr.eseo.atlc.constraints.ReifyExp
import fr.eseo.atlc.constraints.SimpleConstraint
import fr.eseo.atlc.constraints.Strength
import fr.eseo.atlc.constraints.StrengthLevel
import fr.eseo.atlc.constraints.StringLit
import fr.eseo.atlc.constraints.UnaryExp
import fr.eseo.atlc.constraints.UnaryRelExp
import fr.eseo.atlc.constraints.VariableRelationExp
import fr.eseo.atlc.constraints.VariableRelationVectorExp
import fr.eseo.atlc.constraints.VariableVectorExp
import fr.eseo.atol.gen.ATOLGen
import fr.eseo.atol.gen.ATOLGen.CompilationHelper
import fr.eseo.atol.gen.AbstractRule
import fr.eseo.atol.gen.Metaclass
import java.util.function.Supplier
import javafx.scene.Node
import org.eclipse.emf.ecore.EObject
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.m2m.atl.common.ATL.Binding
import org.eclipse.m2m.atl.common.OCL.CollectionExp
import org.eclipse.m2m.atl.common.OCL.IntegerExp
import org.eclipse.m2m.atl.common.OCL.LetExp
import org.eclipse.m2m.atl.common.OCL.NavigationOrAttributeCallExp
import org.eclipse.m2m.atl.common.OCL.OclExpression
import org.eclipse.m2m.atl.common.OCL.OclModelElement
import org.eclipse.m2m.atl.common.OCL.OperationCallExp
import org.eclipse.m2m.atl.common.OCL.PropertyCallExp
import org.eclipse.m2m.atl.common.OCL.RealExp
import org.eclipse.m2m.atl.common.OCL.RealType
import org.eclipse.m2m.atl.common.OCL.StringExp
import org.eclipse.m2m.atl.common.OCL.VariableExp
import org.eclipse.papyrus.aof.core.AOFFactory
import org.eclipse.papyrus.aof.core.IBox
import org.eclipse.papyrus.aof.core.ISequence
import org.eclipse.xtend.lib.annotations.Data
import org.eclipse.xtend2.lib.StringConcatenationClient
import org.eclipse.m2m.atl.common.ATL.PatternElement

@AOFAccessors(ConstraintsPackage)
class Constraints implements ATOLGen.Extension{

	// new "entrypoint"
	override compileBinding(Binding it, extension CompilationHelper compilationHelper) {
		val type = outPatternElement.type as OclModelElement
		val mmClass = type.model.name.mmClass
		val compilerMethodName = '''__«propertyName»Compiler'''.toString
		if(mmClass.declaredMethods.exists[m | m.simpleName == compilerMethodName]) {
			new ConstraintsCompiler(compilationHelper).compile(it)
		} else {
			null
		}
	}

	// old "entrypoint"
	// remark this project requires plugin ...atl.common because of this method
	def StringConcatenationClient __constraintsCompiler(Binding e, CompilationHelper compilationHelper) {
		new ConstraintsCompiler(compilationHelper).compile(e)
	}

	def dispatch String prettyPrint(Constraint it) {
		throw new UnsupportedOperationException('''Cannot pretty display «it»''')
	}

	def dispatch String prettyPrint(SimpleConstraint it) {
		if (arguments.size == 2) {
			'''«strength.prettyPrint» «arguments.get(0).prettyPrint» «predicate» «arguments.get(1).prettyPrint»'''
		}
		else {
			'''«strength.prettyPrint» «predicate»(«FOR a : arguments SEPARATOR ', '»«a.prettyPrint»«ENDFOR»)'''
		}
	}

	def dispatch String prettyPrint(ConstraintGroup it) {
		'''
			{	solver : «solver»
			«FOR c : constraints BEFORE '\t' SEPARATOR ',\n\t'»«c.prettyPrint»«ENDFOR»
			}
		'''
	}

	def dispatch String prettyPrint(UnaryRelExp it) {
		'''«operator» «operand.prettyPrint»'''
	}

	def dispatch String prettyPrint(BinaryRelExp it) {
		'''«left.prettyPrint» «operator» «right.prettyPrint»'''
	}

	def dispatch String prettyPrint(NaryRelExp it) {
		'''«operator»(«FOR o : operands SEPARATOR ', '»«o.prettyPrint»«ENDFOR»)'''
	}

	def dispatch String prettyPrint(Expression it) {
		throw new UnsupportedOperationException('''Cannot pretty display «it»''')
	}

	def dispatch String prettyPrint(UnaryExp it) {
		'''«operator»(«operand.prettyPrint»)'''
	}

	def dispatch String prettyPrint(BinaryExp it) {
		'''«left.prettyPrint» «operator» «right.prettyPrint»'''
	}

	def dispatch String prettyPrint(ReifyExp it) {
		'''reify(«operand.prettyPrint»)'''
	}

	def dispatch String prettyPrint(NaryExp it) {
		'''«operator»(«FOR o : operands SEPARATOR ', '»«o.prettyPrint»«ENDFOR»)'''
	}

	def dispatch String prettyPrint(DoubleExp it) {
		'''«value»'''
	}

	def dispatch String prettyPrint(IntExp it) {
		'''«value»'''
	}

	def dispatch String prettyPrint(StringLit it) {
		'''«value»'''
	}

	def dispatch String prettyPrint(fr.eseo.atlc.constraints.VariableExp it) {
		val src = source
		if (src instanceof Node) {
			'''«src.id».«propertyName»'''
		}
		else if (src instanceof IBox<?>){
			'''box.«propertyName»'''
		}
		else if (src instanceof EObject) {
			'''«src.eResource.getURIFragment(src)».«propertyName»'''
		}
		else {
			'''«src.class.simpleName».«propertyName»'''
		}
	}

	def dispatch String prettyPrint(VariableVectorExp it) {
		'''box.«propertyName»'''
	}

	def dispatch String prettyPrint(VariableRelationExp it) {
		val src = source
		var id = switch (src) {
			Node:
				src.id
			IBox<?>:
				'box'
			EObject:
				src.eResource.getURIFragment(src)
			default:
				src.class.simpleName
		}
		'''«id».{«relationName»}«IF !propertyPath.empty».«propertyPath»«ENDIF»'''
	}

	def dispatch String prettyPrint(VariableRelationVectorExp it) {
		'''box.{«relationName»}.«propertyPath»'''
	}

	def dispatch String prettyPrint(ConstantExp it) {
		val src = source
		if (src instanceof Node) {
			'''[«src.id».«propertyName»]'''
		}
		else if (src instanceof IBox<?>){
			'''[box.«propertyName»]'''
		}
		else if (src instanceof EObject) { //TODO : we can have EObject here (idem for VaraibleExp)
			'''[«src.eResource.getURIFragment(src)».«propertyName»]'''
		}
		else {
			'''[«src.class.simpleName».«propertyName»]'''
		}
	}

	def dispatch String prettyPrint(ConstantVectorExp it) {
		'''[box.«propertyName»]'''
	}

	def dispatch String prettyPrint(Strength it) {
		'''«strength.prettyPrint» «IF weight != 1.0»{«weight»}«ENDIF»'''
	}

	def dispatch String prettyPrint(StrengthLevel it) {
		switch (it) {
			case REQUIRED : '''REQUIRED'''
			case STRONG : '''STRONG'''
			case MEDIUM : '''MEDIUM'''
			case WEAK : '''WEAK'''
		}
	}

	// "Block" metaclass for SimplifyConstraints.atl
	public static val Metaclass<ISequence<Expression>> ExpressionVector = [AOFFactory.INSTANCE.createSequence]

	public val __contents = [ISequence<Expression> it |
		it
	]
}

@Data
class ConstraintsCompiler {
	extension CompilationHelper compilationHelper

	val enableVariableReference = true
	val variableReferences = #{ //TODO : map is not a good structure, it cannot store more than 1 variable reference per class
		'c.period',
		'c.corequisites.target1.period',
		'c.requisites.target.period',
		's.concernedHuman.seat',
		's.referedHuman.seat'
	}

	def StringConcatenationClient compile(Binding it) {
		value.compileEntry(it)
	}

	dispatch def StringConcatenationClient compileEntry(OclExpression e, Binding it) {
		throw new IllegalStateException
	}

	dispatch def isRuleCall(OclExpression it) {
		false
	}

	dispatch def isRuleCall(OperationCallExp it) {
		calledRule !== null
	}

	dispatch def StringConcatenationClient compileEntry(LetExp e, Binding it) {
		val initExpression = e.variable.initExpression
		val isConstraintExp = initExpression.isTarget && !initExpression.isRuleCall
		'''
			«AbstractRule».letStat(
				«if(isConstraintExp) {initExpression.compileExp} else {initExpression.compile}»,
				(«if(isConstraintExp) e.variable.varName else e.variable.mangledVarName») -> {
					«e.in_.compileEntry(it)»
				}
			);
		'''
	}

	dispatch def StringConcatenationClient compileEntry(CollectionExp e, Binding it) {
		//TODO : correctly handle strengths
		'''
			«FOR c : e.elements»
				{
					«outPatternElement.varName».getConstraints().add(«c.compileCons»);
				}
			«ENDFOR»
		'''
	}

	def isStrength(String s) {
		val pattern = "--\\s*(WEAK|MEDIUM|STRONG|REQUIRED)(\\s+\\d+(\\.\\d+)?)?.*"
		s.toUpperCase.matches(pattern)
	}

	def extractStrength(String s) {
		val pattern = "(--\\s*)(WEAK|MEDIUM|STRONG|REQUIRED).*"
		s.toUpperCase.replaceAll(pattern, "$2")
	}

	def extractWeight(String s) {
		val pattern = "(--\\s*[^0-9]*)(\\d+(\\.\\d+)?)?.*" // not 100% certain about this regex, it need proper testing I guess
		val res = s.toUpperCase.replaceAll(pattern, "$2")
		if (res.empty) {
			"1.0"
		}
		else {
			res
		}
	}

// @begin compileCons
	dispatch def compileCons(OclExpression it) {
		throw new UnsupportedOperationException('''Cannot compile constraint «it»''')
	}

	dispatch def StringConcatenationClient compileCons(OperationCallExp it) {
		switch operationName {
			// Compile SimpleConstraint
			case "<",
			case ".<",
			case ">",
			case ".>",
			case ">=",
			case ".>=",
			case "<=",
			case ".<=",
			case "=",
			case ".=",
			case "!=",
			case ".!=",
			case "<>",
			case ".<>",
			case "allDifferent": {
				compileConstraint
			}
			case 'stay': {
				compileStay
			}
			case 'suggest': {
				compileSuggest
			}
			//Compile RelExp
			default: {
				switch arguments.size {
					case 0: compileUnaryRelExp
					case 1: compileBinaryRelExp
					default: compileNaryRelExp
				}
			}
		}
	}
// @end compileCons

	def StringConcatenationClient compileUnaryRelExp(OperationCallExp it) {
		'''
		«AbstractRule».wrap((«Supplier»<«UnaryRelExp»>) () -> {
			«UnaryRelExp» «vn» = «Constraints».UnaryRelExp.newInstance();
			«vn».setOperand(«source.compileCons»);
			«vn».setOperator("«operationName»");
			return «vn»;
		})'''
	}

	def StringConcatenationClient compileBinaryRelExp(OperationCallExp it) {
		'''
		«AbstractRule».wrap((«Supplier»<«BinaryRelExp»>) () -> {
			«BinaryRelExp» «vn» = «Constraints».BinaryRelExp.newInstance();
			«vn».setOperator("«operationName»");
			«vn».setLeft(«source.compileCons»);
			«vn».setRight(«arguments.get(0).compileCons»);
			return «vn»;
		})'''
	}

	def StringConcatenationClient compileNaryRelExp(OperationCallExp it) {
		'''
		«AbstractRule».wrap((«Supplier»<«NaryRelExp»>) () -> {
			«NaryRelExp» «vn» = «Constraints».NaryRelExp.newInstance();
			«vn».setOperator("«operationName»");
			«vn».getOperands().add(«source.compileCons»);
			«FOR s : arguments»
				«vn».getOperands().add(«s.compileCons»);
			«ENDFOR»
			return «vn»;
		})'''
	}

	def StringConcatenationClient compileConstraint(OperationCallExp it) {
		'''
		«AbstractRule».wrap((«Supplier»<«SimpleConstraint»>) () -> {
			«SimpleConstraint» «vn» = «Constraints».SimpleConstraint.newInstance();
			«vn».setPredicate("«operationName»");
			«vn».getArguments().add(«source.compileExp»);
			«FOR a : arguments»
				«vn».getArguments().add(«a.compileExp»);
			«ENDFOR»
			«IF commentsAfter.size > 0 && commentsAfter.get(0).isStrength»
				«vn.compileStrength(commentsAfter.get(0).extractStrength, commentsAfter.get(0).extractWeight)»
			«ELSE»
				if («vn».getStrength() == null) {
					«vn.compileStrength("REQUIRED", "1.0")»
				}
			«ENDIF»
			return «vn»;
		})'''
	}

	def StringConcatenationClient compileStay(OperationCallExp it) {
		// stay(Strength, weight = 1.0)
		if (arguments.size < 1 || arguments.size > 2) {
			throw new UnsupportedOperationException("Stay constraint must have at least one argument and at most two.")
		}
		val s = arguments.get(0).compileValue.toUpperCase
		val w = if (arguments.size == 2) arguments.get(1).compileValue else "1.0".toString
		'''
			«AbstractRule».wrap((«Supplier»<«SimpleConstraint»>) () -> {
				«SimpleConstraint» «vn» = «Constraints».SimpleConstraint.newInstance();
				«vn».setPredicate("«operationName»");
				«vn.compileStrength(s, w)»
				«vn».getArguments().add(«source.compileExp»);
				return «vn»;
			})
		'''
	}

	def StringConcatenationClient compileSuggest(OperationCallExp it) {
		'''
			«AbstractRule».wrap((«Supplier»<«SimpleConstraint»>) () -> {
				«SimpleConstraint» «vn» = «Constraints».SimpleConstraint.newInstance();
				«vn».setPredicate("«operationName»");
				«vn.compileStrength('WEAK','1.0')»
				«vn».getArguments().add(«source.compileExp»);
				«vn».getArguments().add(«arguments.get(0).compileExp»);
				return «vn»;
			})
		'''
	}

	def StringConcatenationClient compileStrength(String vn, String s, String w) {
		'''
			«vn».setStrength(«AbstractRule».wrap((«Supplier»<«Strength»>) () -> {
				«Strength» s_«vn» = «Constraints».Strength.newInstance();
				s_«vn».setStrength(«StrengthLevel».«s»);
				s_«vn».setWeight(«w»);
				return s_«vn»;
			}));
		'''
	}


// @begin compileExp
	dispatch def compileExp(OclExpression it) {
		throw new UnsupportedOperationException('''Cannot compile expression «it»''')
	}

	dispatch def StringConcatenationClient compileExp(NavigationOrAttributeCallExp it) {
		//TODO: look for variable references and compile differently
		val path = propertyPath
		// TODO: isThisModule with mutable helper
		'''
		«AbstractRule».wrap((«Supplier»<«ArExp»>) () -> {
			«IF source.isThisModule && !name.findAttributeHelper.initExpression.isMutable»
				«DoubleExp» «vn» = «Constraints».DoubleExp.newInstance();
				«vn».setValue(«name»);
			«ELSEIF path.hasVariableReference»
				«IF it.sourceExpression instanceof VariableExp»
					«VariableRelationExp» «vn» = «Constraints».VariableRelationExp.newInstance();
				«ELSE»
					«VariableRelationVectorExp» «vn» = «Constraints».VariableRelationVectorExp.newInstance();
				«ENDIF»
				«vn».setSource(«it.sourceExpression.compile»);
				«vn».setPropertyPath("«path.split('\\.').dropWhile[path.variableRelation != it].drop(1).join('.')»");
				«vn».setRelationName("«path.variableRelation»");
			«ELSE»
				«IF it.source instanceof VariableExp && !source.isMutable»
					«fr.eseo.atlc.constraints.VariableExp» «vn» = «Constraints».VariableExp.newInstance();
				«ELSE»
					«VariableVectorExp» «vn» = «Constraints».VariableVectorExp.newInstance();
				«ENDIF»
				«vn».setSource(«it.source.compile»);
				«vn».setPropertyName("«it.name»");
			«ENDIF»
			return «vn»;
		})'''
	}

	def OclExpression getSourceExpression(NavigationOrAttributeCallExp it) {
		val path = propertyPath
		val match = path.variableRelationMatch
		val distance = path.split('\\.').size - match.split('\\.').size
		var ret = source
		for (var i=0; i < distance; i++) {
			switch (ret) {
				PropertyCallExp: {
					ret = ret.source
				}
				default: {
					throw new UnsupportedOperationException('''Unexpected type of navigation in «it».''')
				}
			}
		}
		ret
	}

	def String getPropertyPath(NavigationOrAttributeCallExp it) {
		//TODO: does not work when there are OperationCallExp inside the path, in this case what is the path anyway ?
		val s = source
		switch (s) {
			VariableExp: {
				'''«s.referredVariable.varName».«name»'''
			}
			NavigationOrAttributeCallExp: {
				'''«s.propertyPath».«name»'''
			}
		}
	}

	def boolean hasVariableReference(String path) {
		return enableVariableReference &&
			variableReferences.filter[
				path.startsWith(it)
			].size > 0
	}

	def String getVariableRelation(String path) {
		path.getVariableRelationMatch.split('\\.').last
	}

	def String getVariableRelationMatch(String path) {
		variableReferences.filter[
				path.startsWith(it)
			].last
	}

	dispatch def StringConcatenationClient compileExp(VariableExp it) {
//		'''
//		«AbstractRule».wrap((«Supplier»<«fr.eseo.atlc.constraints.VariableExp»>) () -> {
//			«fr.eseo.atlc.constraints.VariableExp» «vn» = «Constraints».VariableExp.newInstance();
//			«vn».setSource(«referredVariable.varName»);
//			return «vn»;
//		})'''
		if(referredVariable.letExp === null) {
			'''
				«AbstractRule».wrap((«Supplier»<«DoubleExp»>) () -> {
					«DoubleExp» «vn» = «Constraints».DoubleExp.newInstance();
					«vn».setValue(«referredVariable.varName»);
					return «vn»;
				})'''
		} else {
			'''
				«EcoreUtil».copy(«referredVariable.varName»)'''
		}
	}

	dispatch def StringConcatenationClient compileExp(StringExp it) {
		'''"«stringSymbol»"'''
	}

	def String vn(EObject it) {
		'''c_«it.hashCode»''' // TODO: use something like nextInt in a non§static context
	}

	dispatch def StringConcatenationClient compileExp(IntegerExp it) {
		vn.compileConstant(integerSymbol.toString)
	}

	dispatch def StringConcatenationClient compileExp(RealExp it) {
		vn.compileConstant(realSymbol.toString)
	}

	dispatch def StringConcatenationClient compileExp(OperationCallExp it) {
		switch (operationName) {
			case 'toConstant' : {
				if (source instanceof NavigationOrAttributeCallExp) {
					val s = source as NavigationOrAttributeCallExp
					'''
					«AbstractRule».wrap((«Supplier»<«ArExp»>) () -> {
						«IF s.source instanceof VariableExp»
							«ConstantExp» «vn» = «Constraints».ConstantExp.newInstance();
						«ELSE»
							«ConstantVectorExp» «vn» = «Constraints».ConstantVectorExp.newInstance();
						«ENDIF»
						«vn».setSource(«s.source.compile»);
						«vn».setPropertyName("«s.name»");
						return «vn»;
					})'''
				}
				else {
					val s = source as VariableExp
					'''
					«AbstractRule».wrap((«Supplier»<«ArExp»>) () -> {
						«ConstantExp» «vn» = «Constraints».ConstantExp.newInstance();
						«vn».setSource(«s.compile»);
						«vn».setPropertyName(null);
						return «vn»;
					})
					'''
				}
			}

			case 'neg',
			case 'abs',
			case 'product',
			case 'sum': {
				'''
				«AbstractRule».wrap((«Supplier»<«UnaryExp»>) () -> {
					«UnaryExp» «vn» = «Constraints».UnaryExp.newInstance();
					«vn».setOperator("«operationName»");
					«vn».setOperand(«source.compileExp»);
					return «vn»;
				})'''
			}

			case '+',
			case '.+',
			case '-',
			case '.-',
			case '*',
			case '.*',
			case '/',
			case './',
			case '.^',
			case '^',
			case 'mod',
			case 'pow',
			case 'dist',
			case 'min',
			case 'max': {
				'''
				«AbstractRule».wrap((«Supplier»<«BinaryExp»>) () -> {
					«BinaryExp» «vn» = «Constraints».BinaryExp.newInstance();
					«vn».setOperator("«operationName»");
					«vn».setLeft(«source.compileExp»);
					«vn».setRight(«arguments.get(0).compileExp»);
					return «vn»;
				})'''
			}
			case 'reify': {
				'''
				«AbstractRule».wrap((«Supplier»<«ReifyExp»>) () -> {
					«ReifyExp» «vn» = «Constraints».ReifyExp.newInstance();
					«vn».setOperand(«source.compileCons»);
					return «vn»;
				})'''
			}
			default:
				throw new UnsupportedOperationException('''Unkonwn operation «operationName» at «location»''')
		}
	}
// @end compileExp

// @begin compileValue
	dispatch def compileValue(OclExpression it) {
		throw new UnsupportedOperationException('''Cannot compile value «it»''')
	}

	dispatch def String compileValue(StringExp it) {
		stringSymbol
	}

	dispatch def String compileValue(IntegerExp it) {
		integerSymbol.toString
	}

	dispatch def String compileValue(RealExp it) {
		realSymbol.toString
	}
// @end compileValue

	def StringConcatenationClient compileConstant(String vn, String v) {
		try {
			Integer.parseInt(v)
			'''
			«AbstractRule».wrap((«Supplier»<«IntExp»>) () -> {
				«IntExp» «vn» = «Constraints».IntExp.newInstance();
				«vn».setValue(«v»);
				return «vn»;
			})'''
		} catch(NumberFormatException e) {
			Double.parseDouble(v)
			'''
			«AbstractRule».wrap((«Supplier»<«DoubleExp»>) () -> {
				«DoubleExp» «vn» = «Constraints».DoubleExp.newInstance();
				«vn».setValue(«v»);
				return «vn»;
			})'''
		}
	}
}
