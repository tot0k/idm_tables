package fr.eseo.aof.extensions

import java.util.WeakHashMap
import java.util.function.BiConsumer
import org.eclipse.emf.common.notify.Notifier
import org.eclipse.papyrus.aof.core.IBox
import org.eclipse.papyrus.aof.core.IMetaClass
import org.eclipse.papyrus.aof.core.IOne
import org.eclipse.papyrus.aof.core.IUnaryFunction
import org.eclipse.papyrus.aof.core.impl.utils.DefaultObserver

interface AOFExtensions {
	def sum(IBox<Integer> b) {
		new Sum(b, 0, [$0+$1], [$0-$1]).result as IOne<Integer>
	}
	def sumFloats(IBox<Float> b) {
		new Sum(b, 0.0f, [$0+$1], [$0-$1]).result as IOne<Float>
	}
	def <C> allContents(Notifier n, IMetaClass<C> c) {
		AllContents._allContents(n, c)
	}
	def <E> take(IBox<E> b, int n) {
		new Take(b, n).result
	}
	def <E> drop(IBox<E> b, int n) {
		new Drop(b, n).result
	}
	def <E> subSequence(IBox<E> b, int l, int u) {
		if(l > 1) {
			b.drop(l - 1)
		}
		b.take(u - l + 1)
	}
	def <E> sortedBy(IBox<E> b, IUnaryFunction<E, IBox<? extends Comparable<?>>>...bodies) {
		new SortedBy(b, bodies as IUnaryFunction<?, ?>[] as IUnaryFunction<E, IOne<? extends Comparable<?>>>[]).result
	}
	def <E, F> onceForEach(IBox<E> source, (E)=>F added, BiConsumer<E, F> removed, BiConsumer<E, F> readded) {
		val cache = new WeakHashMap
		source.forEach[
			// TODO: should we check for duplicates? or prevent duplicates from being in source box
			cache.put(it, added.apply(it))
		]
		source.addObserver(new DefaultObserver<E> {
			override added(int index, E element) {
				val f = cache.get(element)
				if(f === null) {
					cache.put(element, added.apply(element))
				} else {
					readded.accept(element, f)
				}
			}
			override moved(int newIndex, int oldIndex, E element) {
			}
			override removed(int index, E element) {
				val f = cache.get(element)
				removed.accept(element, f)
			}
			override replaced(int index, E newElement, E oldElement) {
			}
		})
	}
	def <E, K> selectBy(IBox<E> source, IBox<K> key, IUnaryFunction<E, IBox<K>> collector) {
		new SelectBy(source, key.asOne(null))[collector.apply(it).asOne(null)].result
	}
	def <E> reverse(IBox<E> it) {
		new Reverse(it).result
	}

	def <E> emptyBAG() {
		IBox.BAG as IBox<E>
	}

	def <E> emptyOrderedSet() {
		IBox.ORDERED_SET as IBox<E>
	}

	def <E> emptySequence() {
		IBox.SEQUENCE as IBox<E>
	}

	def <E> emptySet() {
		IBox.SET as IBox<E>
	}

	def <E> emptyOption() {
		IBox.OPTION as IBox<E>
	}

	def <E> emptyOne() {
		IBox.ONE as IBox<E>
	}
}