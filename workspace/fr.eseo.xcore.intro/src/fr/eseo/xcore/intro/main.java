package fr.eseo.xcore.intro;

import java.io.IOException;
import java.util.Collections;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EcorePackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import fr.eseo.xcore.intro.impl.IntroFactoryImpl;

public class main {
	
	public static ResourceSet initRS() {
		ResourceSet rs = new ResourceSetImpl();
		rs.getPackageRegistry().put(EcorePackage.eNS_URI, EcorePackage.eINSTANCE);
		rs.getPackageRegistry().put(IntroPackage.eNS_URI, IntroPackage.eINSTANCE);
		rs.getResourceFactoryRegistry().getExtensionToFactoryMap().put("xmi", new XMIResourceFactoryImpl());
		return rs;
	}
	
	public static void showCourse(Course c) {
		System.out.println("Teacher of course " + c.getName() + " : " + c.getTeacher().getName());
		System.out.print("Students of course " + c.getName() + " : ");
		for (Student s: c.getStudents()) {
			System.out.print(s.getName() + " ");
		}
		System.out.println("\n");
	}
	
	public static void showStudent(Student s) {
		System.out.print("Student " + s.getName() + " goes to these courses : ");
		for (Course c: s.getCourses()) {
			System.out.print(c.getName() + " ");
		}
		System.out.println("");
	}
	
	public static void showTeacher(Teacher t) {
		System.out.print("Teacher " + t.getName() + " manages these courses : ");
		for (Course c: t.getCourses()) {
			System.out.print(c.getName() + " ");
		}
		System.out.println("");
	}
	
	public static void main(String[] args) throws IOException {
		IntroFactoryImpl factory = new IntroFactoryImpl();
		
		Teacher fred = factory.createTeacher();
		Teacher jerome = factory.createTeacher();
		fred.setName("Frédéric Jouault");
		jerome.setName("Jérôme Delatour");
		
		Course c1 = factory.createCourse();
		Course c2 = factory.createCourse();
		c1.setName("IDM");
		c1.setTeacher(fred);
		
		c2.setName("OS_embarqué");
		c2.setTeacher(jerome);
		
		Student cravic = factory.createStudent();
		Student laine = factory.createStudent();
		
		cravic.setName("Cravic");
		laine.setName("Lainé");
	
		cravic.getCourses().add(c1);
		c1.getStudents().add(laine);
		c2.getStudents().add(cravic);
		
		showCourse(c1);
		showCourse(c2);
		showStudent(cravic);
		showStudent(laine);
		showTeacher(jerome);
		showTeacher(fred);
		
		ResourceSet rs = initRS();
		Resource r = rs.createResource(URI.createFileURI("test.xmi"));
		r.getContents().add(c1);
		r.getContents().add(c2);
		r.getContents().add(laine);
		r.getContents().add(cravic);
		r.getContents().add(jerome);
		r.getContents().add(fred);
		r.save(Collections.emptyMap());
		
	}
}
