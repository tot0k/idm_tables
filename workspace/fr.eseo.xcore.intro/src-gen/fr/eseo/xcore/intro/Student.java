/**
 */
package fr.eseo.xcore.intro;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Student</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.eseo.xcore.intro.Student#getName <em>Name</em>}</li>
 *   <li>{@link fr.eseo.xcore.intro.Student#getCourses <em>Courses</em>}</li>
 * </ul>
 *
 * @see fr.eseo.xcore.intro.IntroPackage#getStudent()
 * @model
 * @generated
 */
public interface Student extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.eseo.xcore.intro.IntroPackage#getStudent_Name()
	 * @model unique="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.eseo.xcore.intro.Student#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Courses</b></em>' reference list.
	 * The list contents are of type {@link fr.eseo.xcore.intro.Course}.
	 * It is bidirectional and its opposite is '{@link fr.eseo.xcore.intro.Course#getStudents <em>Students</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Courses</em>' reference list.
	 * @see fr.eseo.xcore.intro.IntroPackage#getStudent_Courses()
	 * @see fr.eseo.xcore.intro.Course#getStudents
	 * @model opposite="students"
	 * @generated
	 */
	EList<Course> getCourses();

} // Student
