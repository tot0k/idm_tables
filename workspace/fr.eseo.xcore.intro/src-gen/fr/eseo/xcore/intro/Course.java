/**
 */
package fr.eseo.xcore.intro;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Course</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.eseo.xcore.intro.Course#getName <em>Name</em>}</li>
 *   <li>{@link fr.eseo.xcore.intro.Course#getTeacher <em>Teacher</em>}</li>
 *   <li>{@link fr.eseo.xcore.intro.Course#getStudents <em>Students</em>}</li>
 * </ul>
 *
 * @see fr.eseo.xcore.intro.IntroPackage#getCourse()
 * @model
 * @generated
 */
public interface Course extends EObject {
	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see fr.eseo.xcore.intro.IntroPackage#getCourse_Name()
	 * @model unique="false"
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link fr.eseo.xcore.intro.Course#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Teacher</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link fr.eseo.xcore.intro.Teacher#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Teacher</em>' reference.
	 * @see #setTeacher(Teacher)
	 * @see fr.eseo.xcore.intro.IntroPackage#getCourse_Teacher()
	 * @see fr.eseo.xcore.intro.Teacher#getCourses
	 * @model opposite="courses"
	 * @generated
	 */
	Teacher getTeacher();

	/**
	 * Sets the value of the '{@link fr.eseo.xcore.intro.Course#getTeacher <em>Teacher</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Teacher</em>' reference.
	 * @see #getTeacher()
	 * @generated
	 */
	void setTeacher(Teacher value);

	/**
	 * Returns the value of the '<em><b>Students</b></em>' reference list.
	 * The list contents are of type {@link fr.eseo.xcore.intro.Student}.
	 * It is bidirectional and its opposite is '{@link fr.eseo.xcore.intro.Student#getCourses <em>Courses</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Students</em>' reference list.
	 * @see fr.eseo.xcore.intro.IntroPackage#getCourse_Students()
	 * @see fr.eseo.xcore.intro.Student#getCourses
	 * @model opposite="courses"
	 * @generated
	 */
	EList<Student> getStudents();

} // Course
