package fr.eseo.atlc.example.umlclassobject

import fr.eseo.aof.xtend.utils.AOFAccessors
import fr.eseo.atol.gen.AbstractRule
import java.util.HashMap
import javafx.scene.shape.Circle
import org.eclipse.papyrus.aof.core.AOFFactory
import org.eclipse.papyrus.aof.core.IBox
import org.eclipse.uml2.uml.Generalization
import org.eclipse.uml2.uml.UMLPackage

@AOFAccessors(UMLPackage)
class UML {
	val waypointsCache = new HashMap<Generalization, IBox<Circle>>
	def _waypoints(Generalization it) {
		waypointsCache.computeIfAbsent(it)[AOFFactory.INSTANCE.createSequence]
	}

	def waypoints(IBox<Generalization> it) {
		collectMutable[it?._waypoints ?: AbstractRule.emptySequence]
	}
}