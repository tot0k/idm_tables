# IDM - Plan de table

> Thomas CRAVIC et Thomas LAINÉ

## Objectifs du projet

Le but est de créer un logiciel de gestion de plan de table : Lors de l'organisation d'un événement, il est difficile de créer un plan de table suivant les désirs de chacun (A ne veut pas être à la table de B, mais veut être à coté de C). Ce logiciel aurait une interface graphique, permettrait de créer des tables de tailles variées, des convives, et de spécifier les contraintes entre convives. Il calculerait ensuite le meilleur placement compte tenu de ces contraintes.



## Ce qui a été fait

- Création et affichage de tables avec des chaises
- Déplacement des tables et chaises
- Renommer les tables
- Suppression de tables et chaises
- Suppression des chaises d'une table et réindexation + re-placement des autres chaises présentes
- Affichage des infos des tables
- Création de Guests
- Enregistrement / Chargement du modèle
- Raccourcis clavier / Barre de menu
- Modification des paramètres des tables par le menu contextuel
- Calcul des contraintes pour des cas simples



## Ce qu'il reste à faire

- Afficher les informations d'un Guest
- Affecter une contrainte entre deux Guests
- Afficher un Guest sur la chaise correspondante



## Le projet

### Qu'est-ce qu'une table ?

Une table est définie par :

- Sa forme
- Le nombre de places
- La disposition des places



Pour chaque côté de la table, on devra expliciter le nombre de places.

Pour l'instant, nous créerons uniquement des tables **rectangulaires** ou **rondes**.

Une table ronde n'a pas de bout de table et a un seul côté.
Une table rectangulaire peut avoir 0 (cas du carré) ou 2 bouts de tables.

#### Contraintes

Une contrainte définit une liaison entre deux humains. Si l'humain A est contraint à l'humain B, alors l'humain B est contraint à l'humain A par la contrainte correspondante.

Exception : la contrainte END_OF_TABLE contraint un seul humain.

> La contrainte END_OF_TABLE ne sera pas implémentée dans un premier temps.

| Contrainte   | Contrainte correspondante |
| ------------ | ------------------------- |
| FACING       | FACING                    |
| LEFT_OF      | RIGHT_OF                  |
| RIGHT_OF     | LEFT_OF                   |
| NEXT_TO      | NEXT_TO                   |
| SAME_TABLE   | SAME_TABLE                |
| END_OF_TABLE | -                         |

**Exemple :**

Si on crée les deux contraintes suivantes : `A LEFT_OF B` et `C SAME_TABLE B`

On peut en déduire toutes les contraintes suivantes :

`A LEFT_OF B` 
`B RIGHT_OF A ` 
`A NEXT_TO B`
`B NEXT_TO A`
`A SAME_TABLE B`
`B SAME_TABLE_A`
`C SAME_TABLE B` 
`B SAME_TABLE C`
`C SAME_TABLE A` 
`A SAME_TABLE C`

On peut hiérarchiser les contraintes.
`LEFT_OF` et `RIGHT_OF` impliquent `NEXT_TO`

`FACING` implique `SAME_TABLE`

`NEXT_TO` implique `SAME_TABLE

Cependant il n'est pas possible d'implémenter ces relations actuellement.



Contraintes entre humain référence et humain concerné.

**Contraintes liées à la contrainte SAME_TABLE**

- La table de l'humain référence et de l'humain concerné est la même.

**Contraintes liées à la contrainte END_OF_TABLE**

NON TRAITE POUR LES TABLES RONDES.

**Contraintes liées à la contrainte NEXT_TO**

- Humain référence et humain concerné sur la même table.
- abs(Position humain ref - position humain concerné) = 1 ou nbre de seats sur la table

**Contraintes liées à la contrainte RIGHT_OF**

- position humain ref - position humain concerné = -1 ou nbre seats sur la table

**Contraintes liées à la contrainte LEFT_OF**

- position humain ref - position humain concerné = 1 ou - nbre seats sur la table

**Contraintes liées à la contrainte FACING :**

- La table de l'humain référence est la même que l'humain concerné.

- La nombre de sièges de la table est pair.

- Le siège de l'humain référence est à égale distance (en nombre de sièges) du siège de l'humain concerné par chacun des côtés.

  

## Aspect graphique
=======

#### Aspect graphique

Le logiciel permet la génération d'un plan de table via une fenêtre graphique.

On y retrouve 2 zones : 

	- A gauche :  la zone de dessin affichant le plan de table généré ainsi que le placement de chaque convive.
	- A droite : la boîte à outils permettant de modifier le plan de table.

![](./img/IHM.png)

#### <u>La zone de dessin :</u>

Les tables générées sont affichées avec le nombre de chaises requis tout autour. 

Les tables sont numérotées par un nombre commençant à 1. Le nom de la table affiché sera par défaut "Table" suivi de son numéro. Cependant si un nom de table est donné lors de la création de la table ou d'une modification, il sera affiché à la place du nom par défaut.

Les chaises sont numérotées par une lettre commençant à A (de A à H si la table est constituée de 8 places). Elles sont placées dans le sens croissant en se déplaçant dans le sens des aiguilles d'une montre.

#### <u>La boîte à outils :</u>

Elle permet de créer une table, ajouter un invité, voir tous les invités, générer le plan de table, charger ou sauver un  plan, changer les paramètres d'un composant.

##### <u>Zone 1 : Modifications du plan</u> 

###### Add table

Tout d'abord un bouton identifié "Add table" permet d'ajouter une table. Les paramètres d'une table sont les suivants : 

	- Name : le nom de la table.
	- Color : la couleur de la table pour l'affichage.
	- X, Y : La position de la table.
	- Seats : le nombre de places disponibles.
	- Diameter : le diamètre de la table.

Ces paramètres sont modifiables à tout moment en cliquant dans la zone de dessin sur la table voulue. Un zone  "Context menu" affichera alors les paramètres dans la boîte à outils et permettra la lecture et la modification des paramètres.

###### Add guest

Le deuxième bouton permettant l'ajout d'un composant est le bouton "Add guest". Il permet d'ajouter un convive. Les paramètres d'un convive sont les suivants :

	- Name : Le nom du convive

###### Auto-Reload

Une case à cocher permet l'auto-reload ou non. Si la case est cochée la génération du plan de table se fait à chaque modification des paramètres d'un composant ou chaque fois qu'il y en a un d'ajouté. Quand la génération est terminée, le nom des convives est affiché à côté de leur place.

###### Force reload

Ce bouton lance la génération du plan de table manuellement (utile si l'auto-reload n'est pas actif).

###### Load et Save

Ces boutons permettent de charger (load) ou sauver (save) un plan de table existant.

##### <u>Zone 2 : Guest list</u>

Cette zone liste les invités. Seul leur nom est représenté, en cliquant on voit leurs paramètres dans l'onglet contextuel ("context menu").

##### <u>Zone 3 : Context menu</u>

###### Après le clic sur une table

Après avoir cliqué sur une table, s'affichent ses paramètres dans l'onglet contextuel. Chaque paramètre est alors affiché et peut être modifié via un champ de texte.

###### Après le clic sur un invité

Comme pour une table, en cliquant sur un invité dans la guest list, ses paramètres s'affichent dans la fenêtre contextuelle. On peut y retrouver le nom de l'invité ainsi que les contraintes qui lui appartiennent. Un bouton permet l'ajout d'une contrainte. Un autre permet de voir les toutes contraintes faites sur cet invité.

#### Première Version

Pour une première version, nous ne créerons que des tables rondes.
La liste des contraintes à implémenter est donc réduite : 

| Contrainte | Contrainte correspondante |
| ---------- | ------------------------- |
| LEFT_OF    | RIGHT_OF                  |
| RIGHT_OF   | LEFT_OF                   |
| NEXT_TO    | NEXT_TO                   |
| SAME_TABLE | SAME_TABLE                |

![](./img/Model.svg)

Dans la version minimale, on n'applique pas de priorités sur les contraintes : si le modèle ne trouve pas de plan de table les satisfaisant toutes, il s'arrête.

##### Placement des chaises

Les chaises d'une table doivent être placées autour de celle-ci, la suivre quand on déplace la table, et être dans l'ordre.

La position d'une chaise est déterminée par les formules suivantes : 
$$
\theta = \frac{2\pi}{n_{max}} * n\\
x = radius+\frac{chairWidth}{2}) * cos(\theta) + centerX - \frac{chairWidth}{2}\\
y = radius+\frac{chairHeight}{2}) * sin(\theta) + centerY - \frac{chairHeight}{2}
$$
![](./img/placement_chaises.png)



#### Améliorations

Dans un futur fort fort lointain, nous envisageons d'élargir la forme des tables aux tables rectangulaires, et donc d'ajouter les contraintes `FACING` et `END_OF_TABLE`.

Dans cette version, nous pouvons ajouter un système de priorités aux contraintes afin de pouvoir en "sacrifier" certaines pour prioriser les plus importantes et trouver des solutions même dans les cas complexes.

Dans cette version, nous pourrions aussi préciser le nombre de places par côté de table afin de pouvoir créer des placements de tables plus complexes.

Nous pourrions ajouter la possibilité d'exporter le plan de table généré en CSV / JSON.

**Exemple :** Ci-dessous, un placement de tables en angle, on ne peut donc placer personne en bout de table bleue, et on retire un siège sur un côté de la table verte.

![](./img/upgrade_tables.png)

![](./img/model_upgrade.svg)